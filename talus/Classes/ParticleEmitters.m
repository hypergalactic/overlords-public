//
//  ParticleEmitters.m
//  Overlords
//
//  Created by Robert Wasmann on 11-11-15.
//  Copyright 2011 HyperGalactic Games Corporation. All rights reserved.
//

#import "ParticleEmitters.h"

//
// ParticleStars
//
@implementation ParticleStars

-(id) init
{
	return [self initWithTotalParticles:200];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
		// duration
		duration = kCCParticleDurationInfinity;
        
		// Gravity Mode
		self.emitterMode = kCCParticleModeGravity;
        
		// Gravity Mode: gravity
		self.gravity = ccp(0.0f, 0.0f);
		
		// Gravity Mode:  radial
		self.radialAccel = 0.0f;
		self.radialAccelVar = 0.0f;
        
		//  Gravity Mode: speed of particles
        if (IS_IPAD) {
            self.speed = 106.5f;
            self.speedVar = 63.9f;
        } else {
            self.speed = 50.0f;
            self.speedVar = 30.0f;
        }
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width + 10.0f, winSize.height / 2.0f);
		posVar = ccp(0.0f , winSize.height / 2.0f);
		
        // angle
		angle = 180.0f;
		angleVar = 0.0f;
        
		// life of particles
		life = 20.0f;
		lifeVar = 0.0f;
        
        blinking = TRUE;
        MaxBlinkRate = 4.0f;
        
		// emits per frame
		emissionRate = totalParticles/life;
		
		// color of particles
		startColor.r = 0.0f;
		startColor.g = 0.0f;
		startColor.b = 1.0f;
		startColor.a = 1.0f;
		startColorVar.r = 1.0f;
		startColorVar.g = 1.0f;
		startColorVar.b = 0.0f;
		startColorVar.a = 0.0f;
		endColor.r = 0.0f;
		endColor.g = 0.0f;
		endColor.b = 1.0f;
		endColor.a = 1.0f;
		endColorVar.r = 1.0f;
		endColorVar.g = 1.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;
              
        startSizeVar = 0.0f;
        endSize = kCCParticleStartSizeEqualToEndSize;
		// additive
		self.blendAdditive = NO;
        
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
        
    // size, in pixels
    startSize = self.texture.contentSize.width;
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end

//
// ParticleGalaxies
//
@implementation ParticleGalaxies

-(id) init
{
	return [self initWithTotalParticles:100];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
		duration = kCCParticleDurationInfinity;
        
		// Radius Mode
		self.emitterMode = kCCParticleModeRadius;
        
        self.positionType = kCCPositionTypeGrouped;
        
        // Radius Mode
        if (IS_IPAD) {
            self.startRadius = 21.0;
        } else {
            self.startRadius = 10.0;
        }
        
        self.startRadiusVar = 0;
        self.endRadius = 0;
        self.rotatePerSecond = 360;
        self.rotatePerSecondVar = 0;
        
		// angle
		angle = 81;
		angleVar = 360;
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width / 2.0, winSize.height / 2.0);
		//posVar = ccp(0, winSize.height / 2.0);
		
		// life of particles
		life = 0.9;
		lifeVar = 0;
		
		// size, in pixels
        if (IS_IPAD) {
            startSize = 10.65f;
            endSize = 10.65f;
            startSizeVar = 4.26f;
        } else {
            startSize = 5.0f;
            endSize = 5.0f;
            startSizeVar = 2.0f;
        }
		
		// emits per second
		emissionRate = totalParticles/life;
		
		// color of particles
		startColor.r = 0.00f;
		startColor.g = 0.35f;
		startColor.b = 1.0f;
		startColor.a = 0.77f;
		startColorVar.r = 0.0f;
		startColorVar.g = 0.0f;
		startColorVar.b = 0.0f;
		startColorVar.a = 0.0f;
		endColor.r = 1.0f;
		endColor.g = 0.0f;
		endColor.b = 0.0f;
		endColor.a = 0.0f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 1.0f;
		endColorVar.a = 0.0f;
        
		// additive
		self.blendAdditive = YES;
        
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
    
    // size, in pixels
    //startSize = self.texture.contentSize.width;
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end


//
// ParticlePlasmaBall
//
@implementation ParticlePlasmaBall

-(id) init
{
	return [self initWithTotalParticles:300];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
		duration = kCCParticleDurationInfinity;
        
		// Gravity Mode
		self.emitterMode = kCCParticleModeGravity;
        
        self.positionType = kCCPositionTypeGrouped;
        
        
		// Gravity Mode: gravity
		self.gravity = ccp(0.0f,0.0f);
		
		// Gravity Mode: speed of particles
        
        if (IS_IPAD) {
            self.speed = 6.4f;
            self.speedVar = 0.0f;
        } else {
            self.speed = 3.0f;
            self.speedVar = 0.0f;
        }
        
		// Gravity Mode: radial
        if (IS_IPAD) {
            self.radialAccel = -183.2f;
        } else {
            self.radialAccel = -86.0f;
        }
		self.radialAccelVar = 0.0f;
		
		// Gravity Mode: tagential
        if (IS_IPAD) {
            self.tangentialAccel = -83.1f;
        } else {
            self.tangentialAccel = -43.0f;
        }
		self.tangentialAccelVar = 0.0f;
        //self.tangentialVelocity = 0.0f;
        
		// angle
		angle = 90.0f;
		angleVar = 360.0f;
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width / 2.0f, winSize.height / 2.0f);
		//posVar = ccp(0, winSize.height / 2.0);
		
		// life of particles
		life = 1.0f;
		lifeVar = 0.0f;
		
		// size, in pixels
        if (IS_IPAD) {
            startSize = 2.13f;
            startSizeVar = 2.13f;
            endSize = 42.7f;
        } else {
            startSize = 1.0f;
            startSizeVar = 1.0f;
            endSize = 20.0f;
        }
		
		// emits per second
		emissionRate = totalParticles / life;
		
		// color of particles
		startColor.r = 0.13f;
		startColor.g = 0.0f;
		startColor.b = 0.5f;
		startColor.a = 1.0f;
		startColorVar.r = 0.22f;
		startColorVar.g = 0.0f;
		startColorVar.b = 0.5f;
		startColorVar.a = 0.0f;
		endColor.r = 0.10f;
		endColor.g = 0.0f;
		endColor.b = 1.0f;
		endColor.a = 1.0f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.32f;
		endColorVar.b = 0.94f;
		endColorVar.a = 0.0f;
        
		// additive
		self.blendAdditive = YES;
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
    
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end

//
// ParticleBrickExplosion
//
@implementation ParticleBrickExplosion

-(id) init
{
	return [self initWithTotalParticles:508];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
		duration = 0.4f;
        
		// Gravity Mode
		self.emitterMode = kCCParticleModeGravity;
        
        self.positionType = kCCPositionTypeGrouped;
        
        
		// Gravity Mode: gravity
		self.gravity = ccp(0.0f,0.0f);
		
		// Gravity Mode: speed of particles
        
        if (IS_IPAD) {
            self.speed = 558.93f;
            self.speedVar = 14.04f;
        } else {
            self.speed = 262.0f;
            self.speedVar = 6.58f;
        }
        
		// Gravity Mode: radial
        if (IS_IPAD) {
            self.radialAccel = -2130.0f;
        } else {
            self.radialAccel = -1000.0f;
        }
		self.radialAccelVar = 0.0f;
		
		// Gravity Mode: tagential
        if (IS_IPAD) {
            self.tangentialAccel = 8.52f;
        } else {
            self.tangentialAccel = 4.0f;
        }
		self.tangentialAccelVar = 0.0f;
        
		// angle
		angle = 0.0f;
		angleVar = 360.0f;
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width / 2.0f, winSize.height / 2.0f);
		//posVar = ccp(0, winSize.height / 2.0);
		
		// life of particles
		life = 0.461f;
		lifeVar = 0.11f;
		
		// size, in pixels
        if (IS_IPAD) {
            startSize = 72.4f;
            startSizeVar = 63.9f;
            endSize = 0.0f;
        } else {
            startSize = 34.0f;
            startSizeVar = 30.0f;
            endSize = 0.0f;
        }
		
		// emits per second
		emissionRate = totalParticles / life;
		
		// color of particles
		startColor.r = 1.0f;
		startColor.g = 0.18f;
		startColor.b = 0.0f;
		startColor.a = 0.8f;
		startColorVar.r = 0.0f;
		startColorVar.g = 0.0f;
		startColorVar.b = 0.0f;
		startColorVar.a = 0.0f;
		endColor.r = 1.0f;
		endColor.g = 1.0f;
		endColor.b = 1.0f;
		endColor.a = 0.0f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;
        
		// additive
		self.blendAdditive = YES;
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
    
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end

//
// ParticleFinalExplosion
//
@implementation ParticleFinalExplosion

-(id) init
{
	return [self initWithTotalParticles:506];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
		duration = 0.6f;
        
		// Gravity Mode
		self.emitterMode = kCCParticleModeGravity;
        
        self.positionType = kCCPositionTypeGrouped;
        
        
		// Gravity Mode: gravity
		self.gravity = ccp(0.0f,0.0f);
		
		// Gravity Mode: speed of particles
        
        if (IS_IPAD) {
            self.speed = 238.9f;
            self.speedVar = 827.8f;
        } else {
            self.speed = 112.0f;
            self.speedVar = 388.1f;
        }
        
		// Gravity Mode: radial
		self.radialAccel = 0.0f;
		self.radialAccelVar = 0.0f;
		
		// Gravity Mode: tagential
		self.tangentialAccel = 0.0f;
		self.tangentialAccelVar = 0.0f;
        
		// angle
		angle = 0.0f;
		angleVar = 360.0f;
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width / 2.0f, winSize.height / 2.0f);
		//posVar = ccp(0, winSize.height / 2.0);
		
		// life of particles
		life = 0.592f;
		lifeVar = 0.329f;
		
		// size, in pixels
        if (IS_IPAD) {
            startSize = 72.4f;
            startSizeVar = 63.9f;
            endSize = 0.0f;
        } else {
            startSize = 34.0f;
            startSizeVar = 30.0f;
            endSize = 0.0f;
        }
		
		// emits per second
		emissionRate = totalParticles / life;
		
		// color of particles
		startColor.r = 1.0f;
		startColor.g = 0.09f;
		startColor.b = 0.0f;
		startColor.a = 0.0f;
		startColorVar.r = 0.0f;
		startColorVar.g = 0.0f;
		startColorVar.b = 0.0f;
		startColorVar.a = 0.0f;
		endColor.r = 1.0f;
		endColor.g = 1.0f;
		endColor.b = 0.0f;
		endColor.a = 0.0f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;
        
		// additive
		self.blendAdditive = NO;
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
    
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end

//
// ParticleRingExplosion
//
@implementation ParticleRingExplosion

-(id) init
{
	return [self initWithTotalParticles:809];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
		duration = 0.01f;
        
		// Gravity Mode
		self.emitterMode = kCCParticleModeGravity;
        
        self.positionType = kCCPositionTypeGrouped;
        
        
		// Gravity Mode: gravity
        if (IS_IPAD) {
            self.gravity = ccp(2.45f, 3.37f);
        } else {
            self.gravity = ccp(1.15f,1.58f);
        }
		
		// Gravity Mode: speed of particles
        
        if (IS_IPAD) {
            self.speed = 518.3f;
            self.speedVar = 2.13f;
        } else {
            self.speed = 243.0f;
            self.speedVar = 1.0f;
        }
        
		// Gravity Mode: radial
		self.radialAccel = 0.0f;
		self.radialAccelVar = 0.0f;
		
		// Gravity Mode: tagential
		self.tangentialAccel = 0.0f;
		self.tangentialAccelVar = 0.0f;
        
		// angle
		angle = 0.0f;
		angleVar = 360.0f;
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width / 2.0f, winSize.height / 2.0f);
		//posVar = ccp(0, winSize.height / 2.0);
		
		// life of particles
		life = 0.0f;
		lifeVar = 1.118f;
		
		// size, in pixels
        if (IS_IPAD) {
            startSize = 72.4f;
            startSizeVar = 63.9f;
            endSize = 29.9f;
        } else {
            startSize = 34.0f;
            startSizeVar = 30.0f;
            endSize = 14.0f;
        }
		
		// emits per second
		emissionRate = totalParticles / life;
		
		// color of particles
		startColor.r = 0.89f;
		startColor.g = 0.56f;
		startColor.b = 0.36f;
		startColor.a = 1.0f;
		startColorVar.r = 0.2f;
		startColorVar.g = 0.2f;
		startColorVar.b = 0.2f;
		startColorVar.a = 0.5f;
		endColor.r = 0.0f;
		endColor.g = 0.0f;
		endColor.b = 0.0f;
		endColor.a = 0.84f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;
        
		// additive
		self.blendAdditive = YES;
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
    
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end

//
// ParticleElectrons
//
@implementation ParticleElectrons

-(id) init
{
	return [self initWithTotalParticles:100];
}

-(id) initWithTotalParticles:(NSUInteger)p {
    
    if( (self=[super initWithTotalParticles:p]) ) {
        // duration
		duration = kCCParticleDurationInfinity;
        
		// Gravity Mode
		self.emitterMode = kCCParticleModeGravity;
        
        self.positionType = kCCPositionTypeGrouped;
        
        
		// Gravity Mode: gravity
        if (IS_IPAD) {
            self.gravity = ccp(127.8f,0.0f);
        } else {
            self.gravity = ccp(60.0f,0.0f);
        }
		
		// Gravity Mode: speed of particles
        
        if (IS_IPAD) {
            self.speed = 170.4f;
            self.speedVar = 0.0f;
        } else {
            self.speed = 80.0f;
            self.speedVar = 0.0f;
        }
        
		// Gravity Mode: radial
        if (IS_IPAD) {
            self.radialAccel = -468.6f;
        } else {
            self.radialAccel = -220.0f;
        }
		self.radialAccelVar = 0.0f;
		
		// Gravity Mode: tagential
		self.tangentialAccel = 0.0f;
		self.tangentialAccelVar = 0.0f;
        
		// angle
		angle = 90.0f;
		angleVar = 0.0f;
		
		// emitter position
		CGSize winSize = [[CCDirector sharedDirector] winSize];
		self.position = ccp(winSize.width / 2.0f, winSize.height / 2.0f);
		//posVar = ccp(0, winSize.height / 2.0);
		
		// life of particles
		life = 8.0f;
		lifeVar = 0.0f;
		
		// size, in pixels
        if (IS_IPAD) {
            startSize = 10.65f;
            startSizeVar = 4.26f;
            endSize = 6.39f;
        } else {
            startSize = 5.0f;
            startSizeVar = 2.0f;
            endSize = 3.0f;
        }
		
		// emits per second
		emissionRate = totalParticles / life;
		
		// color of particles
		startColor.r = 1.0f;
		startColor.g = 0.0f;
		startColor.b = 0.0f;
		startColor.a = 1.0f;
		startColorVar.r = 0.0f;
		startColorVar.g = 0.0f;
		startColorVar.b = 0.0f;
		startColorVar.a = 0.0f;
		endColor.r = 0.0f;
		endColor.g = 0.0f;
		endColor.b = 1.0f;
		endColor.a = 1.0f;
		endColorVar.r = 0.0f;
		endColorVar.g = 0.0f;
		endColorVar.b = 0.0f;
		endColorVar.a = 0.0f;
        
		// additive
		self.blendAdditive = YES;
	}
	
	return self;
    
}

-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName {
    
    [self initWithTotalParticles:p];
    
    CCTexture2D *tex = [[CCTextureCache sharedTextureCache] addImage:textureName];
    NSAssert( tex, @"ParticleStars: Couldn't load texture");
    if( tex )
        self.texture = tex;
    
    return self;
}


- (void)dealloc
{
	[super dealloc];
}

@end




