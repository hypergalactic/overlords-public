//
//  CCParallaxNode-Extras.h
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import "cocos2d.h"

@interface CCParallaxNode (Extras) 

-(void) incrementOffset:(CGPoint)offset forChild:(CCNode*)node;
-(void) setYOffset:(float)newYoffset forChild:(CCNode*)node;


@end
