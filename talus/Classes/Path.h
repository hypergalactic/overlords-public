//
//  Path.h
//  Overlords
//
//  Created by Robert Wasmann on 11-05-09.
//  Copyright 2011 HyperGalactic Games Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

#define graphicsPath(_path) [Path graphicsPath:_path]
#define gameResourcePath(_path) [Path resourcePath:_path]


@interface Path : NSObject {
}
+(NSString *)graphicsPath:(NSString*)filePath;
+(NSString *)resourcePath:(NSString*)filePath;
@end

