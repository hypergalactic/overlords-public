//
//  Ball.h
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import "cocos2d.h"

@class Shield;
@class ParticlePlasmaBall;

@interface Ball : CCSprite {
@private
    BOOL    active_; // Is the ball in active play?
    BOOL    heldByEnemyShield_;
    BOOL    justHeldByEnemyShield_; // False if any wall or brick bounces occur
    BOOL    heldByPlayerShield_;
    BOOL    justHeldByPlayerShield_; // False if any wall or brick bounces occur
    BOOL    ball1_; // Is this ball1?
    Shield  *shieldHolding_;
	CGPoint velocity_;
    float scalarVelocity_;
    short ticksSinceLastCollision_;
    short brickImpactsTillDeath_; // How many brick impacts before ball dies
    short brickImpacts_; // How many brick impacts is ball capable of
    ParticlePlasmaBall *myPlasmaBall_; // Which plasma ball is associated with this ball sprite
    
}

@property(nonatomic, readwrite, assign) BOOL active;
@property(nonatomic, readwrite, assign) BOOL ball1;
@property(nonatomic, readwrite, assign) BOOL heldByEnemyShield;
@property(nonatomic, readwrite, assign) BOOL heldByPlayerShield;
@property(nonatomic, readwrite, assign) BOOL justHeldByPlayerShield;
@property(nonatomic, readwrite, assign) BOOL justHeldByEnemyShield;
@property(nonatomic, readwrite, assign) CGPoint velocity;
@property(nonatomic, readwrite, assign) float scalarVelocity;
@property(nonatomic, readwrite, assign) short ticksSinceLastCollision;
@property(nonatomic, readwrite, assign) short brickImpactsTillDeath;
@property(nonatomic, readwrite, assign) short brickImpacts;
@property(nonatomic, readonly) float radius;
@property(nonatomic, readwrite, assign) Shield *shieldHolding;
@property(nonatomic, readwrite, assign) ParticlePlasmaBall *myPlasmaBall;


+ (id)ballWithTexture:(CCTexture2D *)texture;
- (float)randomStart;
- (void)reset;
- (void)move:(ccTime)delta;
- (BOOL)collideWithShield:(Shield *)Shield;
- (void)setVelocityAngle:(float)velocityAngle;
- (void)setScalarVelocity:(float)scalarVelocity;
- (void)addXVelocity:(float)xVelocity;
- (void)randomness;
@end
