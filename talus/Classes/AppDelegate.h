//
//  AppDelegate.h
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window_;
	RootViewController	*viewController_;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, assign) RootViewController *viewController;

@end
