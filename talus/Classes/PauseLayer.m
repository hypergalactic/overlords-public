//
//  PauseLayer.m
//  Overlords
//
//  Created by Robert Wasmann on 12-06-18.
//  Copyright (c) 2012 HyperGalactic Games Corporation. All rights reserved.
//
#import "SimpleAudioEngine.h"
#import "PauseLayer.h"
#import "Path.h"

#define controlBoxRatio 0.04166f    //0.0833f  // Height ratio of the controller box


@implementation PauseLayer

@synthesize delegate = delegate_;

+ (id) layerWithColor:(ccColor4B)color delegate:(id)_delegate
{
	return [[[self alloc] initWithColor:color delegate:_delegate] autorelease];
}

- (id) initWithColor:(ccColor4B)c delegate:(id)_delegate {
    self = [super initWithColor:c];
    if (self != nil) {
        
		CGSize winSize = [[CCDirector sharedDirector] winSize];
        
		delegate_ = _delegate;
		[self pauseDelegate];
        
        CCLabelBMFont *resumeLabel;
        resumeLabel = [CCLabelBMFont labelWithString:@"Resume" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
        
        CCMenuItemLabel *resumeItem = [CCMenuItemLabel itemWithLabel:resumeLabel target:self selector:@selector(doResume:)];
        resumeItem.scale = 0.0;
        resumeItem.position = ccp(winSize.width / 2.0, winSize.height * 0.8);
        
        CCLabelBMFont *leaderboardLabel;
        leaderboardLabel = [CCLabelBMFont labelWithString:@"High Scores" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        
        CCMenuItemLabel *leaderboardItem = [CCMenuItemLabel itemWithLabel:leaderboardLabel target:self selector:@selector(doShowLeaderboard)];
        leaderboardItem.scale = 0.0;
        leaderboardItem.position = ccp(winSize.width / 2.0, winSize.height * 0.7);
        
        CCLabelBMFont *achievementsLabel;
        achievementsLabel = [CCLabelBMFont labelWithString:@"Achievements" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        
        CCMenuItemLabel *achievementsItem = [CCMenuItemLabel itemWithLabel:achievementsLabel target:self selector:@selector(doShowAchievements)];
        achievementsItem.scale = 0.0;
        achievementsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
        
        CCLabelBMFont *creditsLabel;
        creditsLabel = [CCLabelBMFont labelWithString:@"Credits" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        
        CCMenuItemLabel *creditsItem = [CCMenuItemLabel itemWithLabel:creditsLabel target:self selector:@selector(creditsTapped:)];
        creditsItem.scale = 0.0;
        creditsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.5);
        
        CCLabelBMFont *soundLabel;
        if ([[SimpleAudioEngine sharedEngine] mute]) {
            soundLabel = [CCLabelBMFont labelWithString:@"Sound on" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
        } else {
            soundLabel = [CCLabelBMFont labelWithString:@"Sound off" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        }
        
        CCMenuItemLabel *soundItem = [CCMenuItemLabel itemWithLabel:soundLabel target:self selector:@selector(doSound:)];
        soundItem.scale = 0.0;
        soundItem.position = ccp(winSize.width / 2.0, winSize.height * 0.4);
        
        CCLabelBMFont *musicLabel;
        if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]) {
            musicLabel = [CCLabelBMFont labelWithString:@"Music on" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        } else {
            musicLabel = [CCLabelBMFont labelWithString:@"Music off" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        }
        
        CCMenuItemLabel *musicItem = [CCMenuItemLabel itemWithLabel:musicLabel target:self selector:@selector(doMusic:)];
        musicItem.scale = 0.0;
        musicItem.position = ccp(winSize.width / 2.0, winSize.height * 0.3);
        
        CCLabelBMFont *restartLabel;
        restartLabel = [CCLabelBMFont labelWithString:@"Reset Game" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        
        CCMenuItemLabel *restartItem = [CCMenuItemLabel itemWithLabel:restartLabel target:self selector:@selector(verifyReset:)];
        restartItem.scale = 0.0;
        restartItem.position = ccp(winSize.width/2, winSize.height * 0.2);
        
        menu_ = [CCMenu menuWithItems:resumeItem, leaderboardItem, achievementsItem, creditsItem, soundItem, musicItem, restartItem, nil];
        menu_.position = CGPointZero;
        [self addChild:menu_ z:5];
        
        [resumeItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [soundItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [musicItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [restartItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [leaderboardItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [achievementsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        [creditsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        
        //id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.1];
        //id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
        //id scaleUp2 = [CCScaleTo actionWithDuration:0.5 scale:1.1];
        //id scaleNorm2 = [CCScaleTo actionWithDuration:0.5 scale:1.0];
        //id scaleUp3 = [CCScaleTo actionWithDuration:0.5 scale:1.1];
        //id scaleNorm3 = [CCScaleTo actionWithDuration:0.5 scale:1.0];
        
        
        //[resumeItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
        //[soundItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp2, scaleNorm2, nil]]];
        //[restartItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp3, scaleNorm3, nil]]];
    }
    return self;
}

- (void)verifyReset:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:menu_ cleanup:TRUE];
    
    // Ask for verification
    NSString *message = @"Are you sure?";
    CCLabelBMFont *label = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label.scale = 0.0;
    label.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    [self addChild:label z:5];
    
    CCLabelBMFont *yesLabel;
    yesLabel = [CCLabelBMFont labelWithString:@"YES" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCLabelBMFont *noLabel;
    noLabel = [CCLabelBMFont labelWithString:@"NO" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCMenuItemLabel *yesItem = [CCMenuItemLabel itemWithLabel:yesLabel target:self selector:@selector(doReset:)];
    yesItem.scale = 0.0;
    yesItem.position = ccp(winSize.width / 3.0, winSize.height * 0.5);
    
    CCMenuItemLabel *noItem = [CCMenuItemLabel itemWithLabel:noLabel target:self selector:@selector(doPauseAgain:)];
    noItem.scale = 0.0;
    noItem.position = ccp(2.0 * winSize.width / 3.0, winSize.height * 0.5);
    
    menu_ = [CCMenu menuWithItems:yesItem, noItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    //[yesItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    //[noItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [label runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.1];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    id scaleUp2 = [CCScaleTo actionWithDuration:0.5 scale:1.1];
    id scaleNorm2 = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    
    [yesItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    [noItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp2, scaleNorm2, nil]]];
}

- (void)creditsTapped:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    float controlBoxHeight = controlBoxRatio * winSize.height;

    [self removeChild:menu_ cleanup:TRUE];
    
    NSString *message = @"(c) 2012 HyperGalactic Games Corp.\nVisit: www.hypergalactic.ca\n\nDesign: Robert Wasmann\n        Dr. Lisa Glass\nProgramming: Robert Wasmann\nArt, Sound:  Robert Wasmann\nMath Wizardry: Dr. Lisa Glass\n\nMusic by BEN (1H1D!!!)\n1H1D!!! Official Site:\nhttp://www.1h1d.net/\n1H1D!!! on Bandcamp:\nhttp://1h1d.bandcamp.com/\n\nSpecial Thanks: You!";
    
    CCLabelBMFont *label;
    CCLabelBMFont *label2;

    label = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label.scale = 0.0;
    label.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    [self addChild:label z:5];
    
    // get the app's version and display it
	NSString *version = [NSString stringWithFormat:@"v%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]];
    label2 = [CCLabelBMFont labelWithString:version fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label2.scale = 0.0;
    // Align left
    [label2 setAnchorPoint: ccp(0.0f, 0.5f)];
    label2.position = ccp(winSize.width/2.0 - winSize.height/3.0 + winSize.height / 96.0, controlBoxHeight / 2.0
                           - label2.contentSize.height / 10.0 + controlBoxHeight);
    [self addChild:label2 z:5];
    
    CCLabelBMFont *optionsLabel;
    optionsLabel = [CCLabelBMFont labelWithString:@"Options" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *optionsItem;
    optionsItem = [CCMenuItemLabel itemWithLabel:optionsLabel target:self selector:@selector(doPauseAgain:)];

    optionsItem.scale = 0.0;
    optionsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.25);
    
    menu_ = [CCMenu menuWithItems:optionsItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    //[optionsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:0.7]];
    [label runAction:[CCScaleTo actionWithDuration:0.5 scale:0.5]];
    [label2 runAction:[CCScaleTo actionWithDuration:0.5 scale:0.5]];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:0.77];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:0.7];
    
    [optionsItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    
}

- (void)musicError:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:menu_ cleanup:TRUE];
    
    CCLabelBMFont *label;
    
    NSString *message = @"Sorry, but you must stop the\nexternal audio player first!";
    
    label = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label.scale = 0.0;
    label.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    [self addChild:label z:5];
    
    CCLabelBMFont *okLabel;
    okLabel = [CCLabelBMFont labelWithString:@"OK" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *okItem;
    okItem = [CCMenuItemLabel itemWithLabel:okLabel target:self selector:@selector(doPauseAgain:)];
    
    okItem.scale = 0.0;
    okItem.position = ccp(winSize.width / 2.0, winSize.height * 0.45);
    
    menu_ = [CCMenu menuWithItems:okItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    //[optionsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:0.7]];
    [label runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.2];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    
    [okItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    
}


-(void)doShowLeaderboard
{
	if([delegate_ respondsToSelector:@selector(showLeaderboard:)])
		[delegate_ showLeaderboard:self];
}

-(void)doShowAchievements
{
	if([delegate_ respondsToSelector:@selector(showAchievements:)])
		[delegate_ showAchievements:self];
}

-(void)pauseDelegate
{
	if([delegate_ respondsToSelector:@selector(pauseLayerDidPause)])
		[delegate_ pauseLayerDidPause];
	[delegate_ onExit];
	[delegate_.parent addChild:self z:10];
}

-(void)doResume: (id)sender
{
	[delegate_ onEnter];
	if([delegate_ respondsToSelector:@selector(pauseLayerDidUnpause)])
		[delegate_ pauseLayerDidUnpause];
	[self.parent removeChild:self cleanup:YES];
}

-(void)doPauseAgain: (id)sender
{
	if([delegate_ respondsToSelector:@selector(doPause)])
		[delegate_ doPause];
	[self.parent removeChild:self cleanup:YES];
}

-(void)doReset: (id)sender
{
	if([delegate_ respondsToSelector:@selector(restartTapped:)])
		[delegate_ restartTapped:self.parent];
	[self.parent removeChild:self cleanup:YES];
}

-(void)doSound: (id)sender
{
	if([delegate_ respondsToSelector:@selector(soundOffTapped:)])
		[delegate_ soundOffTapped:self.parent];
    if([delegate_ respondsToSelector:@selector(doPause)])
		[delegate_ doPause];
	[self.parent removeChild:self cleanup:YES];
}

-(void)doMusic: (id)sender
{
	if([delegate_ respondsToSelector:@selector(musicOffTapped:)]) {
		[delegate_ musicOffTapped:self.parent];
        // If ipod music is playing, display an error advising to shut it off first
        if ([[CDAudioManager sharedManager] isOtherAudioPlaying]) {
            [self musicError:self];
            return;
        }
    }

    if([delegate_ respondsToSelector:@selector(doPause)])
		[delegate_ doPause];
	[self.parent removeChild:self cleanup:YES];
}


-(void)dealloc
{
	[super dealloc];
}

@end
