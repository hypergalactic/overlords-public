//
//  OverlordsScene.m
//  Overlords
//
//  Created by Robert Wasmann on 10-06-28.
//  Copyright HyperGalactic Games Corporation 2010. All rights reserved.
//

#import "OverlordsScene.h"
#import "Shield.h"
#import "Ball.h"
#import "SimpleAudioEngine.h"
#import "CCParallaxNode-Extras.h"
#import "ParticleEmitters.h"
#import "PauseLayer.h"
#import "Path.h"
#import "GCHelper.h"
#import "GameState.h"
#import "AppDelegate.h"
#import "Appirater.h"
#import "AccessDefaults.h"

#include <stdlib.h>
#include <math.h>
#import <GameKit/GameKit.h>

enum tagPlayer {
	kHighPlayer,
	kLowPlayer
} Player;	

#define controlBoxRatio 0.04166f    //0.0833f  // Height ratio of the controller box
#define shieldPosRatio 2.7f     // Shield position as a 1/x ratio of the screen height
#define numSmallStars 800 // Number of background stars to scroll
#define numMediumStars 100 // Number of background stars to scroll
#define numLargeStars 50 // Number of background stars to scroll
#define SJ_PI_X_2 6.28318530718f
#define shipRotateSpeed 360.0f // Degrees per second 
#define shipTime 40 // Number of second between plasmoid spheres
#define countTime 3 // Number of seconds for ship intro countdown
#define startLevel 1 // Level to start single player game at

#define releaseSpeed 300.0f // Speed that ball is initially released from ship
#define _initSlowBallSpeed_ 150.0f // Initial slow ball speed
#define _maxSlowBallSpeed_ 250.0f // Max slow ball speed at peakLevel
#define _initFastBallSpeed_ 250.0f // Initial fast ball speed 
#define _maxFastBallSpeed_ 350.0f // Maximum fast ball speed at peakLevel
#define _initHyperBallSpeed_ 350.0f // Initial hyper speed attainable by human flick
#define _maxHyperBallSpeed_ 450.0f // Max hyper speed attainable by human flick at peakLevel
#define peakLevel 100.0f // Max theoretical level for difficulty curve


@interface OverlordsLayer ()
@end

@implementation OverlordsScene

- (id)init
{
	if ((self = [super init]) == nil) return nil;
	
	OverlordsLayer *overlordsLayer = [OverlordsLayer node];
	[self addChild:overlordsLayer];
	
	return self;
}

- (void)onExit
{
	[super onExit];
}

@end

@implementation OverlordsLayer

- (id)init
{
    lives_ = 2;
    level_ = 20;
    AILevel_ = 20;
    playerScore_ = 0;
    enemyScore_ = 0;
    attractMode_ = TRUE;
    newGame_ = FALSE;
    singlePlayer_ = FALSE;
    easy_ = FALSE;
    brickExplosionNum_ = 1;
    colorDeltaR_ = -1;
    colorDeltaB_ = 2;
    colorDeltaG_ = 1;
    nextTrack_ = 0;
    musicPaused_ = YES;
    nextTrack_ = 0;
    
    self.isTouchEnabled = YES;
    
	if ((self = [super init]) == nil) return nil;
	
	[[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // What OS version are we running on?
    sysVersion_ = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    loadingScreen_ = [CCSprite spriteWithFile:graphicsPath(@"Default.png")];
    loadingScreen_.position = ccp(winSize.width / 2.0, winSize.height / 2.0);
    [self addChild:loadingScreen_ z:10];

	// calculate the scaled height for the control area
	controlBoxHeight_ = controlBoxRatio * winSize.height; 
    
    // Determine initial ball speeds based on level
    [self setCurrentBallSpeed];
	
    // Pre-load sounds and music
    
    SimpleAudioEngine *sae = [SimpleAudioEngine sharedEngine];
    if (sae != nil) {
        [[CDAudioManager sharedManager] setMode:kAMM_FxPlusMusicIfNoOtherAudio];
        [[CDAudioManager sharedManager] setResignBehavior:kAMRBStopPlay autoHandle:YES];
        [sae preloadBackgroundMusic:gameResourcePath(@"sound/Synchronicity-128-aac.caf")];
        if (sae.willPlayBackgroundMusic) {
            sae.backgroundMusicVolume = 0.7f;
        }
    }
    
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/blip.caf")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/ship-fly-in-160.mp3")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/ship-fly-out-160.mp3")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/ship-cruising-160.mp3")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/brick-explosion2.caf")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/shield-bounce3.caf")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/shield-grab2.caf")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/WilhelmScream.caf")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/final-explosion3.caf")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/intro-alien-voiceover.mp3")];
    [[SimpleAudioEngine sharedEngine] preloadEffect:gameResourcePath(@"sound/nice-move-voiceover.mp3")];
    
	[self addRedBorder];
        
    // Show player score
    NSString *playerScore = [NSString stringWithFormat:@"%d", playerScore_];
    playerScoreLabel_ = [CCLabelBMFont labelWithString:playerScore fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    playerScoreLabel_.scale = 0.5;
    // Align right
    [playerScoreLabel_ setAnchorPoint: ccp(1.0f, 0.5f)];
    playerScoreLabel_.position = ccp(winSize.width / 2.0 + winSize.height / 3.0 - winSize.height / 96.0, controlBoxHeight_ / 2.0 
                                     - playerScoreLabel_.contentSize.height / 10.0);
    [self addChild:playerScoreLabel_ z:5];
    
    // Show enemy score
    NSString *enemyScore = [NSString stringWithFormat:@"%d", enemyScore_];
    enemyScoreLabel_ = [CCLabelBMFont labelWithString:enemyScore fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    enemyScoreLabel_.scale = 0.5;
    // Align right
    [enemyScoreLabel_ setAnchorPoint: ccp(1.0f, 0.5f)];
    [enemyScoreLabel_ setRotation:180];
    enemyScoreLabel_.position = ccp(winSize.width/2.0 - winSize.height/3.0 + winSize.height / 96.0, winSize.height - controlBoxHeight_ / 2.0 
                                     + playerScoreLabel_.contentSize.height / 10.0);
    [self addChild:enemyScoreLabel_ z:5];
    	
	ball_ = [Ball ballWithTexture:[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"snow.png")]];
	ball_.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
    [ball_ setColor:ccc3(0, 0, 0)];
    [ball_ setOpacity:0];
    ball_.ball1 = TRUE;
    ball_.brickImpacts = 1000;
	[self addChild:ball_ z:4];
    
    ball2_ = [Ball ballWithTexture:[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"snow.png")]];
	ball2_.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
    [ball2_ setColor:ccc3(0, 0, 0)];
    [ball2_ setOpacity:0];
    ball2_.brickImpacts = 6;
	[self addChild:ball2_ z:4];
    
    ball3_ = [Ball ballWithTexture:[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"snow.png")]];
	ball3_.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
    [ball3_ setColor:ccc3(0, 0, 0)];
    [ball3_ setOpacity:0];
    ball3_.brickImpacts = 1;
	[self addChild:ball3_ z:4];
    
    ball4_ = [Ball ballWithTexture:[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"snow.png")]];
	ball4_.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
    [ball4_ setColor:ccc3(0, 0, 0)];
    [ball4_ setOpacity:0];
    ball4_.brickImpacts = 1;
	[self addChild:ball4_ z:4];
    
    ball5_ = [Ball ballWithTexture:[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"snow.png")]];
	ball5_.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
    [ball5_ setColor:ccc3(0, 0, 0)];
    [ball5_ setOpacity:0];
    ball5_.brickImpacts = 1;
	[self addChild:ball5_ z:4];
    
    ball6_ = [Ball ballWithTexture:[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"snow.png")]];
	ball6_.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
    [ball6_ setColor:ccc3(0, 0, 0)];
    [ball6_ setOpacity:0];
    ball6_.brickImpacts = 1;
	[self addChild:ball6_ z:4];
	
    NSMutableArray *ballsM = [NSMutableArray arrayWithCapacity:6];
    [ballsM addObject:ball_];
    [ballsM addObject:ball2_];
    [ballsM addObject:ball3_];
    [ballsM addObject:ball4_];
    [ballsM addObject:ball5_];
    [ballsM addObject:ball6_];

    balls_ = [ballsM copy];
    ballsM = nil;
    
	shieldTexture_ = [[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"Shield4.png")] retain];
    shieldTexture180_ = [[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"Shield4-180.png")] retain];
    shieldTextureGlow_ = [[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"Shield4-glow.png")] retain];
    
    //[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"fireball2.png")];
		
	playerShield_ = [Shield shieldWithTexture:shieldTexture_ inverted:FALSE];
	playerShield_.position = CGPointMake(winSize.width / 2.0, winSize.height / shieldPosRatio);
    [playerShield_ setColor:ccc3(0, 255, 200)];
	[self addChild:playerShield_ z:1];
	
	enemyShield_ = [Shield shieldWithTexture:shieldTexture180_ inverted:TRUE];
	enemyShield_.position = CGPointMake(winSize.width / 2.0, winSize.height - winSize.height / shieldPosRatio);
    [enemyShield_ setColor:ccc3(0, 255, 200)];
	[self addChild:enemyShield_ z:1];
    
    // Show player magnetron power level
    NSString *playerMagLvl = [NSString stringWithFormat:@"%d%%", playerShield_.magnetronPower];
    playerMagLvlLabel_ = [CCLabelBMFont labelWithString:playerMagLvl fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    playerMagLvlLabel_.scale = 0.5;
    // Align center
    [playerMagLvlLabel_ setAnchorPoint: ccp(0.5f, 0.5f)];
    playerMagLvlLabel_.position = ccp(winSize.width / 2.0, controlBoxHeight_ / 2.0 
                                     - playerMagLvlLabel_.contentSize.height / 10.0);
    [self addChild:playerMagLvlLabel_ z:5];
    
    // Show enemy magnetron power level
    NSString *enemyMagLvl = [NSString stringWithFormat:@"%d%%", enemyShield_.magnetronPower];
    enemyMagLvlLabel_ = [CCLabelBMFont labelWithString:enemyMagLvl fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    enemyMagLvlLabel_.scale = 0.5;
    // Align center
    [enemyMagLvlLabel_ setAnchorPoint: ccp(0.5f, 0.5f)];
    [enemyMagLvlLabel_ setRotation:180];
    enemyMagLvlLabel_.position = ccp(winSize.width / 2.0, winSize.height - controlBoxHeight_ / 2.0 
                                     + enemyMagLvlLabel_.contentSize.height / 10.0);
    [self addChild:enemyMagLvlLabel_ z:5];
    
    [playerShield_ setAiPlayer:TRUE];
    [enemyShield_ setAiPlayer:TRUE];
    
    // Initialize alien ship
    
    alienScrunchTexture_ = [[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"alien-scrunched.png")] retain];
    alienRelaxedTexture_ = [[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"alien.png")] retain];
    alienSpinningTexture_ = [[[CCTextureCache sharedTextureCache] addImage:graphicsPath(@"alien-openeye.png")] retain];

    ship_ = [CCSprite spriteWithTexture:alienRelaxedTexture_];
    ship_.position = CGPointMake(0 - ship_.texture.contentSize.width, winSize.height / 2.0);
    [self addChild:ship_ z:3];
    ship_.visible = FALSE;

    //[self showMainMenu:self];

    [self drawBricks];
    
    // Add brick explosion emitter 508 particles
    brickExplosion_ =[[ParticleBrickExplosion alloc] 
                    initWithTotalParticles:300 withTexture:graphicsPath(@"fireball2.png")];
    [self addChild:brickExplosion_ z:1];
    [brickExplosion_ stopSystem];
    
    brickExplosion2_ =[[ParticleBrickExplosion alloc] 
                      initWithTotalParticles:300 withTexture:graphicsPath(@"fireball2.png")];
    [self addChild:brickExplosion2_ z:1];
    [brickExplosion2_ stopSystem];
    
    // Add final explosion emitter
    finalExplosion_ =[[ParticleFinalExplosion alloc] 
                      initWithTotalParticles:500 withTexture:graphicsPath(@"fireball2.png")];
    [self addChild:finalExplosion_ z:1];
    [finalExplosion_ stopSystem];
    
    // Add final ring explosion emitter 800 particles
    ringExplosion_ =[[ParticleRingExplosion alloc] 
                      initWithTotalParticles:800 withTexture:graphicsPath(@"fireball2.png")];
    [self addChild:ringExplosion_ z:1];
    [ringExplosion_ stopSystem];
    
    // Add player magnetron energy electrons emitter
    electrons_ =[[ParticleElectrons alloc] 
                     initWithTotalParticles:75 withTexture:graphicsPath(@"star-large.png")];
    if (IS_IPAD) {
        electrons_.position = ccp(winSize.width / 2.0 - 8, controlBoxHeight_ + 42.6);
    } else {
        electrons_.position = ccp(winSize.width / 2.0 - 4, controlBoxHeight_ + 20.0);
    }
    [self addChild:electrons_ z:0];
    [electrons_ stopSystem];
    
    // Add enemy magnetron energy electrons emitter
    electrons2_ =[[ParticleElectrons alloc] 
                     initWithTotalParticles:75 withTexture:graphicsPath(@"star-large.png")];
    if (IS_IPAD) {
        electrons2_.position = ccp(winSize.width / 2.0 + 8, winSize.height - controlBoxHeight_ - 42.6);
    } else {
        electrons2_.position = ccp(winSize.width / 2.0 + 4, winSize.height - controlBoxHeight_ - 20);
    }
    electrons2_.rotation = 180.0;
    [self addChild:electrons2_ z:0];
    [electrons2_ stopSystem];

    
    // Add scrolling star particle engine effects

    scrollingStars1_ = [[ParticleStars alloc] initWithTotalParticles:numSmallStars 
                        withTexture:graphicsPath(@"star-small.png")];
    [self addChild:scrollingStars1_ z:-2];
    
    scrollingStars2_ = [[ParticleStars alloc] initWithTotalParticles:numMediumStars 
                        withTexture:graphicsPath(@"star-medium.png")];
    [self addChild:scrollingStars2_ z:-2];
    
    scrollingStars3_ = [[ParticleStars alloc] initWithTotalParticles:numLargeStars 
                        withTexture:graphicsPath(@"star-large.png")];
    [self addChild:scrollingStars3_ z:-2];
    
    // Inital star emitters that fill entire screen and then scroll off
    
    scrollingStars4_ = [[ParticleStars alloc] initWithTotalParticles:numSmallStars 
                        withTexture:graphicsPath(@"star-small.png")];
    scrollingStars4_.position = ccp(winSize.width / 2.0, winSize.height / 2.0);
    scrollingStars4_.posVar = ccp(winSize.width / 2.0, winSize.height / 2.0);
    scrollingStars4_.duration = 0.1f;
    scrollingStars4_.emissionRate = numSmallStars * 2.0f;
    scrollingStars4_.autoRemoveOnFinish = TRUE;
    [self addChild:scrollingStars4_ z:-2];
    
    scrollingStars5_ = [[ParticleStars alloc] initWithTotalParticles:numMediumStars 
                        withTexture:graphicsPath(@"star-medium.png")];
    scrollingStars5_.position = ccp(winSize.width / 2.0, winSize.height / 2.0);
    scrollingStars5_.posVar = ccp(winSize.width / 2.0, winSize.height / 2.0);
    scrollingStars5_.duration = 0.1f;
    scrollingStars5_.emissionRate = numMediumStars * 2.0f;
    scrollingStars5_.autoRemoveOnFinish = TRUE;
    [self addChild:scrollingStars5_ z:-2];
    
    scrollingStars6_ = [[ParticleStars alloc] initWithTotalParticles:numLargeStars 
                        withTexture:graphicsPath(@"star-large.png")];
    scrollingStars6_.position = ccp(winSize.width / 2.0, winSize.height / 2.0);
    scrollingStars6_.posVar = ccp(winSize.width / 2.0, winSize.height / 2.0);
    scrollingStars6_.duration = 0.1f;
    scrollingStars6_.emissionRate = numLargeStars * 2.0f;
    scrollingStars6_.autoRemoveOnFinish = TRUE;
    [self addChild:scrollingStars6_ z:-2];
    
    
    // Add Plasmoids
    plasmaBall_ = [[ParticlePlasmaBall alloc] initWithTotalParticles:25 
                                        withTexture:graphicsPath(@"star-large.png")];
    [self addChild:plasmaBall_ z:4];
    plasmaBall_.visible = FALSE;
    ball_.visible = FALSE;
    ball_.myPlasmaBall = plasmaBall_;
    [plasmaBall_ stopSystem];
    
    plasmaBall2_ = [[ParticlePlasmaBall alloc] initWithTotalParticles:25 
                                                         withTexture:graphicsPath(@"star-large.png")];
    [self addChild:plasmaBall2_ z:4];
    plasmaBall2_.visible = FALSE;
    ball2_.visible = FALSE;
    ball2_.myPlasmaBall = plasmaBall2_;
    [plasmaBall2_ stopSystem];
    
    plasmaBall3_ = [[ParticlePlasmaBall alloc] initWithTotalParticles:25
                                                          withTexture:graphicsPath(@"star-large.png")];
    [self addChild:plasmaBall3_ z:4];
    plasmaBall3_.visible = FALSE;
    ball3_.visible = FALSE;
    ball3_.myPlasmaBall = plasmaBall3_;
    [plasmaBall3_ stopSystem];
    
    plasmaBall4_ = [[ParticlePlasmaBall alloc] initWithTotalParticles:25
                                                          withTexture:graphicsPath(@"star-large.png")];
    [self addChild:plasmaBall4_ z:4];
    plasmaBall4_.visible = FALSE;
    ball4_.visible = FALSE;
    ball4_.myPlasmaBall = plasmaBall4_;
    [plasmaBall4_ stopSystem];
    
    plasmaBall5_ = [[ParticlePlasmaBall alloc] initWithTotalParticles:25
                                                          withTexture:graphicsPath(@"star-large.png")];
    [self addChild:plasmaBall5_ z:4];
    plasmaBall5_.visible = FALSE;
    ball5_.visible = FALSE;
    ball5_.myPlasmaBall = plasmaBall5_;
    [plasmaBall5_ stopSystem];
    
    plasmaBall6_ = [[ParticlePlasmaBall alloc] initWithTotalParticles:25
                                                          withTexture:graphicsPath(@"star-large.png")];
    [self addChild:plasmaBall6_ z:4];
    plasmaBall6_.visible = FALSE;
    ball6_.visible = FALSE;
    ball6_.myPlasmaBall = plasmaBall6_;
    [plasmaBall6_ stopSystem];
    
	[self schedule:@selector(scheduleGameStart) interval:6];
    
	return self;
}

- (void)scheduleGameStart {
    [self schedule:@selector(doStep:)];
    [self removeChild:loadingScreen_ cleanup:TRUE];
    [self showMainMenu:self];
    [self newGame];
    //[[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/Synchronicity-128-aac.caf") loop:YES];
    //[[CDAudioManager sharedManager] setBackgroundMusicCompletionListener:self selector:@selector(selectNextBGTrack)];
    musicPaused_ = NO;  // Start music
    // Read in musicPaused state
    musicPaused_ = [AccessDefaults defaultGetMusicPaused];
    if (musicPaused_) {
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
    [self unschedule:@selector(scheduleGameStart)];
}

- (void)scheduleNewGame {
    [self newGame];
    [self unschedule:@selector(scheduleNewGame)];
}


- (void)doStep:(ccTime)delta
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
            
	if (attractMode_) {
        [enemyShield_ calcNextMove:balls_ currLevel:level_ dt:delta score:(unsigned int)playerScore_ enemyPosition:(CGPoint)playerShield_.position
                  enemyHoldingBall:playerShield_.holdingBall easy:easy_];
        [playerShield_ calcNextMove:balls_ currLevel:level_ dt:delta score:(unsigned int)enemyScore_ enemyPosition:(CGPoint)enemyShield_.position
                        enemyHoldingBall:enemyShield_.holdingBall easy:easy_];
    }
    
    if (playerShield_.state == kShieldStateMagnetronOn && playerGlow_ == FALSE) {
        [playerShield_ setTexture:shieldTextureGlow_];
        [playerShield_ setTextureRect:[playerShield_ rect]];
        playerGlow_ = TRUE;
    } else if (playerShield_.state != kShieldStateMagnetronOn && playerGlow_ == TRUE) {
        [playerShield_ setTexture:shieldTexture_];
        [playerShield_ setTextureRect:[playerShield_ rect]];
        playerGlow_ = FALSE;
    }
    
    if (enemyShield_.state == kShieldStateMagnetronOn && enemyGlow_ == FALSE) {
        [enemyShield_ setTexture:shieldTextureGlow_];
        [enemyShield_ setTextureRect:[enemyShield_ rect]];
        enemyShield_.rotation = 180.0;
        enemyGlow_ = TRUE;
    } else if (enemyShield_.state != kShieldStateMagnetronOn && enemyGlow_ == TRUE) {
        [enemyShield_ setTexture:shieldTexture180_];
        [enemyShield_ setTextureRect:[enemyShield_ rect]];
        enemyShield_.rotation = 0;
        enemyGlow_ = FALSE;
    }
    
    for (Ball *ball in balls_) {
        
        if (!ball.visible) continue;

        if ([ball collideWithShield:playerShield_]) {
            if (playerShield_.state == kShieldStateMagnetronOn && playerShield_.holdingBall == FALSE && playerShield_.visible) {
                playerShield_.holdingBall = TRUE;
                ball.heldByPlayerShield = TRUE;
                ball.shieldHolding = playerShield_;
                lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/shield-grab2.caf")];
            } else if (playerShield_.visible) {
                // Play sound
                lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/shield-bounce3.caf")];
                
                // Increase ball speed 25%
                float ballSpeed = ball.scalarVelocity * 1.5;
                if (ballSpeed > fastBallSpeed_) ballSpeed = fastBallSpeed_;
                [ball setScalarVelocity:ballSpeed];
            }
        }
        if ([ball collideWithShield:enemyShield_]) {
            if (enemyShield_.state == kShieldStateMagnetronOn && enemyShield_.holdingBall == FALSE && enemyShield_.visible) {
                enemyShield_.holdingBall = TRUE;
                ball.heldByEnemyShield = TRUE;
                ball.shieldHolding = enemyShield_;
                lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/shield-grab2.caf")];
            } else if (enemyShield_.visible) {
                // Play sound
                lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/shield-bounce3.caf")];
                
                // Increase ball speed 25%
                float ballSpeed = ball.scalarVelocity * 1.5;
                if (ballSpeed > fastBallSpeed_) ballSpeed = fastBallSpeed_;
                [ball setScalarVelocity:ballSpeed];
            }
        }
        // Check if ball collides with Greemo (ship)
        // Collision check
        if ((ccpLengthSQ(ccpSub(ball.position, ship_.position)) < (ball.contentSize.width / 2.0 +
            ship_.contentSize.width / 4.0)*(ball.contentSize.width / 2.0 + ship_.contentSize.width / 4.0)) && ball.active){
                        
            // Play sound
            lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/brick-explosion2.caf")];
            
            // Use a brick explosion that isn't in use
            if (brickExplosionNum_ == 1) {
                brickExplosion_.position = ship_.position;
                brickExplosion_.visible = TRUE;
                [brickExplosion_ resetSystem];
                brickExplosionNum_ = 2;
            } else {
                brickExplosion2_.position = ship_.position;
                brickExplosion2_.visible = TRUE;
                [brickExplosion2_ resetSystem];
                brickExplosionNum_ = 1;
            }
            
            float velocityAngle = CC_DEGREES_TO_RADIANS(50.0f); // 50 degrees
            
            if (ball.justHeldByEnemyShield || ball.justHeldByPlayerShield) {
                // Set the non active balls flying
                if (ball.justHeldByEnemyShield) {
                    enemyScore_ = enemyScore_ + 999;
                    [enemyScoreLabel_ setString:[NSString stringWithFormat:@"%d", enemyScore_]];
                    velocityAngle += M_PI;
                } else {
                    playerScore_ = playerScore_ + 999;
                    [playerScoreLabel_ setString:[NSString stringWithFormat:@"%d", playerScore_]];
                }
                if (ball2_.visible) {
                    velocityAngle += CC_DEGREES_TO_RADIANS(10.0f);
                    ball2_.brickImpactsTillDeath = 6;
                }
                for (Ball *theBall in balls_) {
                    if (!theBall.visible && !theBall.ball1) {
                        theBall.position = ship_.position;
                        [theBall setVelocityAngle:velocityAngle];
                        [theBall setScalarVelocity:fastBallSpeed_];
                        theBall.visible = TRUE;
                        theBall.active = TRUE;
                        theBall.myPlasmaBall.visible = TRUE;
                        velocityAngle += CC_DEGREES_TO_RADIANS(20.0f); // 20 degrees
                    }
                }
                ball.justHeldByEnemyShield = FALSE;
                ball.justHeldByPlayerShield = FALSE;
            }
            [self resetGreemo];
        }
    }
    
    // Check map bricks for ball_ collisions
    for (Ball *ball in balls_) {
        
        if (!ball.visible) continue;
        
        // Increase counter on each ball used to stall brick collision checking after collisions
        ball.ticksSinceLastCollision += 1;

        if ((ball.position.y < controlBoxHeight_ + winSize.height/shieldPosRatio) && ball.ticksSinceLastCollision >= 3){

            CGSize playerLayerSize = [playerLayer_ layerSize];
            for( int x=0; x < playerLayerSize.width;x++) {
                if (ball.ticksSinceLastCollision == 0) break;
                for( int y=0; y < playerLayerSize.height; y++ ) {
                    if (ball.ticksSinceLastCollision == 0) break;
                    CCSprite *brick = [playerLayer_ tileAt:ccp(x,y)];    
                    if (brick == NULL || brick.visible == FALSE) continue;
                    CGPoint brickWorldPos = [playerMap_ convertToWorldSpace:brick.position];
                    
                    // Collision check
                    if (ccpLengthSQ(ccpSub(ball.position, brickWorldPos)) < (ball.contentSize.width / 2.2 + 
                        brick.contentSize.height / 2.0)*(ball.contentSize.width / 2.2 + brick.contentSize.height / 2.0)){
                        
                        // Check for collision with inner core bricks marked endGame == True    
                        int tileGid = [playerLayer_ tileGIDAt:ccp(x,y)];
                        if (tileGid) {
                            NSDictionary *properties = [playerMap_ propertiesForGID:tileGid];
                            if (properties) {
                                NSString *endGame = [properties valueForKey:@"endGame"];
                                if (endGame && [endGame compare:@"True"] == NSOrderedSame) {
                                    if (playerBlown_) {
                                        continue; // Skip endGame bricks if player side has already been blown up
                                    }
                                    roundOver_ = TRUE;
                                    playerBlown_ = TRUE;

                                    lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/final-explosion3.caf")];
                                    lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/WilhelmScream.caf")];
                                    
                                    if (IS_IPAD) {
                                        finalExplosion_.position = ccp(winSize.width / 2.0, controlBoxHeight_ + 42.6);
                                    } else {
                                        finalExplosion_.position = ccp(winSize.width / 2.0, controlBoxHeight_ + 20.0);
                                    }
                                    finalExplosion_.visible = TRUE;
                                    [finalExplosion_ resetSystem];
                                    
                                    if (IS_IPAD) {
                                        ringExplosion_.position = ccp(winSize.width / 2.0, controlBoxHeight_ + 42.6);
                                    } else {
                                        ringExplosion_.position = ccp(winSize.width / 2.0, controlBoxHeight_ + 20.0);
                                    }
                                    ringExplosion_.visible = TRUE;
                                    [ringExplosion_ resetSystem];
                                    
                                    electrons_.visible = FALSE;
                                    
                                    // Remove shield since dead
                                    playerShield_.visible = FALSE;
                                    
                                    // Adjust Ball speed to minimum
                                    [ball setScalarVelocity:slowBallSpeed_];
                                    ball.ticksSinceLastCollision = 0;
                                    
                                    ball.brickImpactsTillDeath--;
                                    ball.justHeldByPlayerShield = FALSE;
                                    ball.justHeldByEnemyShield = FALSE;
                                    
                                    if (enemyBlown_) continue;
                                    
                                    enemyScore_ = enemyScore_ + 6666;
                                    [enemyScoreLabel_ setString:[NSString stringWithFormat:@"%d", enemyScore_]];
                           
                                    if (singlePlayer_) {
                                        lives_--;
                                    }
                                    if (singlePlayer_ && lives_ < 0) {
                                        [playerLivesLabel_ setString:@"Lives:0"];
                                        endReason_ = kEndReasonLose;
                                        gameOver_ = TRUE;
                                        [self endScene:self];
                                        break; // Break out of loop checking for more brick collisions
                                    } else if (singlePlayer_ && lives_ >= 0) {
                                        endReason_ = kEndReasonLose;
                                        [self endScene:self];
                                        break; // Break out of loop checking for more brick collisions
                                    } else if (twoPlayer_) {
                                        endReason_ = kEndReasonWin;
                                        level_ = level_ + 10;
                                        if (level_ > 100) {
                                            level_ = 100;
                                        }
                                        [self endScene:self];
                                        break; // Break out of loop checking for more brick collisions
                                    } else {
                                        [self schedule:@selector(scheduleNewGame) interval:5];
                                        break; // Break out of loop checking for more brick collisions
                                    }
                                }
                            }
                        }
                        // Play sound
                        lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/brick-explosion2.caf")];
   
                        // Blow up the brick!
                        if (brickExplosionNum_ == 1) {
                            brickExplosion_.position = brickWorldPos;
                            brickExplosion_.visible = TRUE;
                            [brickExplosion_ resetSystem];
                            brickExplosionNum_ = 2;
                        } else {
                            brickExplosion2_.position = brickWorldPos;
                            brickExplosion2_.visible = TRUE;
                            [brickExplosion2_ resetSystem];
                            brickExplosionNum_ = 1;
                        }
                        
                        ball.brickImpactsTillDeath--;
                        ball.justHeldByPlayerShield = FALSE;
                        ball.justHeldByEnemyShield = FALSE;
                        
                        if (!roundOver_) {
                            enemyScore_ = enemyScore_ + 125;
                            [enemyScoreLabel_ setString:[NSString stringWithFormat:@"%d", enemyScore_]];
                        }
       
                        // Reduce AI difficulty
                        if (AILevel_ > 1 && AILevel_ > level_ - 5 && !twoPlayer_) {
                            AILevel_ = AILevel_ - 1;
                            [self setCurrentBallSpeed];
                        }
                        
                        brick.visible = FALSE;
                        // Skip one tick to prevent case of taking out two bricks over two consecutive ticks
                        ball.ticksSinceLastCollision = 0; 
                        // Adjust Ball speed to minimum
                        [ball setScalarVelocity:slowBallSpeed_];
                        ball.velocity = ccpNeg(ball.velocity);
                        break; // Break out of loop checking for more brick collisions
                    }
                }
            }
        }
        
        //Check Enemy Map bricks for ball collisions
        if ((ball.position.y > winSize.height - controlBoxHeight_ - winSize.height/shieldPosRatio) && ball.ticksSinceLastCollision >= 3){
            CGSize enemyLayerSize = [enemyLayer_ layerSize];
            for( int x=0; x < enemyLayerSize.width;x++) {
                if (ball.ticksSinceLastCollision == 0) break;
                for( int y=0; y < enemyLayerSize.height; y++ ) {
                    if (ball.ticksSinceLastCollision == 0) break;

                    CCSprite *brick = [enemyLayer_ tileAt:ccp(x,y)];
                    if (brick == NULL || brick.visible == FALSE) continue;
                    //CGPoint brickWorldPos = ccpAdd(brickWorldOffset, brick.position);
                    CGPoint brickWorldPos = [enemyMap_ convertToWorldSpace:brick.position];
                    
                    // Collision check
                    if (ccpLengthSQ(ccpSub(ball.position, brickWorldPos)) < (ball.contentSize.width / 2.2 + 
                        brick.contentSize.height / 2.2)*(ball.contentSize.width / 2.2 + brick.contentSize.height / 2.2)){
                        
                        // Check for collision with inner core bricks marked endGame == True    
                        int tileGid = [enemyLayer_ tileGIDAt:ccp(x,y)];
                        if (tileGid) {
                            NSDictionary *properties = [enemyMap_ propertiesForGID:tileGid];
                            if (properties) {
                                NSString *endGame = [properties valueForKey:@"endGame"];
                                if (endGame && [endGame compare:@"True"] == NSOrderedSame) {
                                    if (enemyBlown_) {
                                        continue; // Skip endGame bricks if enemy side has already been blown up
                                    }
                                    roundOver_ = TRUE;
                                    enemyBlown_ = TRUE;
                                    
                                    lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/final-explosion3.caf")];
                                    lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/WilhelmScream.caf")];
                                    
                                    if (IS_IPAD) {
                                        finalExplosion_.position = ccp(winSize.width / 2.0, winSize.height - controlBoxHeight_ - 42.6);
                                    } else {
                                        finalExplosion_.position = ccp(winSize.width / 2.0, winSize.height - controlBoxHeight_ - 20.0);
                                    }
                                    finalExplosion_.visible = TRUE;
                                    [finalExplosion_ resetSystem];
                                    
                                    if (IS_IPAD) {
                                        ringExplosion_.position = ccp(winSize.width / 2.0, winSize.height - controlBoxHeight_ - 42.6);
                                    } else {
                                        ringExplosion_.position = ccp(winSize.width / 2.0, winSize.height - controlBoxHeight_ - 20.0);
                                    }
                                    ringExplosion_.visible = TRUE;
                                    [ringExplosion_ resetSystem];
                                    
                                    electrons2_.visible = FALSE;

                                    // Remove shield since dead
                                    enemyShield_.visible = FALSE;
                                    
                                    // Adjust Ball speed to minimum
                                    [ball setScalarVelocity:slowBallSpeed_];
                                    ball.ticksSinceLastCollision = 0;
                                    
                                    ball.brickImpactsTillDeath--;
                                    ball.justHeldByPlayerShield = FALSE;
                                    ball.justHeldByEnemyShield = FALSE;
                                    
                                    if (playerBlown_) continue;

                                    playerScore_ = playerScore_ + 6666;
                                    [playerScoreLabel_ setString:[NSString stringWithFormat:@"%d", playerScore_]];
                                    
                                    if (singlePlayer_) {
                                        level_++;
                                        endReason_ = kEndReasonWin;
                                        [self endScene:self];
                                        break; // Break out of loop checking for more brick collisions
                                    } else if (twoPlayer_) {
                                        endReason_ = kEndReasonWin;
                                        level_ = level_ + 10;
                                        if (level_ > 100) {
                                            level_ = 100;
                                        }
                                        [self endScene:self];
                                        break; // Break out of loop checking for more brick collisions
                                    }
                                    [self schedule:@selector(scheduleNewGame) interval:5];
                                    break; // Break out of loop checking for more brick collisions
                                }
                            }
                        }
                        // Play sound
                        lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/brick-explosion2.caf")];
                        
                        // Blow up the brick!
                        if (brickExplosionNum_ == 1) {
                            brickExplosion_.position = brickWorldPos;
                            brickExplosion_.visible = TRUE;
                            [brickExplosion_ resetSystem];
                            brickExplosionNum_ = 2;
                        } else {
                            brickExplosion2_.position = brickWorldPos;
                            brickExplosion2_.visible = TRUE;
                            [brickExplosion2_ resetSystem];
                            brickExplosionNum_ = 1;
                        }
                        
                        ball.brickImpactsTillDeath--;
                        ball.justHeldByPlayerShield = FALSE;
                        ball.justHeldByEnemyShield = FALSE;

                        if (!roundOver_) {
                            playerScore_ = playerScore_ + 125;
                            [playerScoreLabel_ setString:[NSString stringWithFormat:@"%d", playerScore_]];
                        }
                        
                        // Increase AI Difficulty
                        if ((AILevel_ < level_ + 5 && !twoPlayer_) && !easy_) {
                            AILevel_++;
                            [self setCurrentBallSpeed];
                        } else if ((AILevel_ < level_ + 2 && !twoPlayer_) && easy_) {
                            AILevel_++;
                            [self setCurrentBallSpeed];
                        }
                        
                        brick.visible = FALSE;
                        // Skip one tick to prevent case of taking out two bricks over two consecutive ticks
                        ball.ticksSinceLastCollision = 0;
                        // Adjust Ball speed to minimum
                        [ball setScalarVelocity:slowBallSpeed_];
                        ball.velocity = ccpNeg(ball.velocity);
                        break; // Break out of loop checking for more brick collisions
                    }
                }
            }
        }
    }
    
    for (Ball *ball in balls_) {
        if (ball.brickImpactsTillDeath <= 0) {
            if (ball.ball1) {
                ball.brickImpactsTillDeath++;
                continue;
            }
            [ball stopAllActions];
            ball.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
            [ball reset];
            ball.myPlasmaBall.visible = FALSE;
            [ball.myPlasmaBall stopSystem];
            [ball.myPlasmaBall resetSystem];
        }
    }
    
    [playerShield_ update:delta];
    [playerShield_ move];

    // If single player game then determine AI's next move
    [enemyShield_ update:delta];
    if (singlePlayer_) {
        
        [enemyShield_ calcNextMove:balls_ currLevel:AILevel_ dt:delta score:(unsigned int)playerScore_ enemyPosition:(CGPoint)playerShield_.position
                  enemyHoldingBall:playerShield_.holdingBall easy:easy_];
    }
    [enemyShield_ move];

   
    for (Ball *ball in balls_) {
        
        if (!ball.visible) continue;

        if (((playerShield_.holdingBall && ball.heldByPlayerShield && playerShield_.state == kShieldStateMagnetronOn && playerShield_.visible) ||
             (enemyShield_.holdingBall && ball.heldByEnemyShield && enemyShield_.state == kShieldStateMagnetronOn && enemyShield_.visible))) {
            if (playerShield_.holdingBall && ball.heldByPlayerShield) {
                ball.position = playerShield_.position;
            } else if (enemyShield_.holdingBall && ball.heldByEnemyShield) {
                ball.position = enemyShield_.position;
            }
        } else if ((playerShield_.holdingBall && ball.heldByPlayerShield && (playerShield_.state != kShieldStateMagnetronOn || playerShield_.visible == FALSE) ) || 
                   (enemyShield_.holdingBall && ball.heldByEnemyShield && (enemyShield_.state != kShieldStateMagnetronOn || enemyShield_.visible == FALSE))) {
            if (playerShield_.holdingBall && ball.heldByPlayerShield) {
                [ball setVelocityAngle:playerShield_.angle];
                [ball setScalarVelocity:fastBallSpeed_];
                [ball addXVelocity:playerShield_.xVel];
                playerShield_.holdingBall = FALSE;
                ball.heldByPlayerShield = FALSE;
                ball.shieldHolding = nil;
                lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/shield-bounce3.caf")];

            } else if (enemyShield_.holdingBall && ball.heldByEnemyShield) {
                [ball setVelocityAngle:enemyShield_.angle];
                [ball setScalarVelocity:fastBallSpeed_];
                [ball addXVelocity:enemyShield_.xVel];
                enemyShield_.holdingBall = FALSE;
                ball.heldByEnemyShield = FALSE;
                ball.shieldHolding = nil;
                lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/shield-bounce3.caf")];

            }
            
            if (ball.scalarVelocity > hyperBallSpeed_) [ball setScalarVelocity:hyperBallSpeed_];
                
        } else if (ball.visible) {
            [ball move:delta];
        }
    }

    // Align plasma sphere emitter and ball_ position
    // Alignment is needed for versions at or below 4.2
    
    for (Ball *ball in balls_) {
        if (sysVersion_ <= 4.2) {
            CGPoint tmp = ball.position;
            
            if (IS_IPAD) {
                tmp.x += 4.26f;
                tmp.y += -4.26f;
            } else {
                tmp.x += 2.0f;
                tmp.y += -2.0f;
            }
            ball.myPlasmaBall.position = tmp;
        } else {
            ball.myPlasmaBall.position = ball.position;
        }
    }
    
    // Update magnetron power label 
    accumulatedTime_ = accumulatedTime_ + delta; 
    if (accumulatedTime_ > 0.1) {
        [playerMagLvlLabel_ setString:[NSString stringWithFormat:@"%d%%", playerShield_.magnetronPower]];
        [enemyMagLvlLabel_ setString:[NSString stringWithFormat:@"%d%%", enemyShield_.magnetronPower]];
        accumulatedTime_ = 0.0;
    }

    //Update the alien's animation frame
    alienAnimationTime_ = alienAnimationTime_ + delta;
    if (!alienSpinning_) {
        if (alienSpinningTextureDisplayed_) {
            [ship_ setTexture:alienScrunchTexture_];
            alienSpinningTextureDisplayed_ = FALSE;
        }
        if (!alienTextureFlipped_ && alienAnimationTime_ > 0.2) {
            ship_.flipX = TRUE;
            alienAnimationTime_ = 0;
            alienTextureFlipped_ = TRUE;
        } else if (alienTextureFlipped_ && alienAnimationTime_ > 0.2) {
            ship_.flipX = FALSE;
            alienAnimationTime_ = 0;
            alienTextureFlipped_ = FALSE;
        }
    } else if (alienSpinning_) {
        if (!alienSpinningTextureDisplayed_) {
            [ship_ setTexture:alienSpinningTexture_];
            alienSpinningTextureDisplayed_ = TRUE;
        }
        alienAnimationTime_ = 0;
    }
    
    // Change the border color slowly over time.
    

    int r = (int)borderColor_.r;
    int b = (int)borderColor_.b;
    int g = (int)borderColor_.g;

    colorTime_ += delta;
    
    if (colorTime_ > 0.1f) {

        if (redIncreasing_) {
            if (r + colorDeltaR_ >= 255) {
                redIncreasing_ = FALSE;
                borderColor_.r = 255;
                colorDeltaR_ *= -1;
            }
        } else {
            if (r + colorDeltaR_ <= 0) {
                redIncreasing_ = TRUE;
                borderColor_.r = 0;
                colorDeltaR_ *= -1;
            }
        }
            
        if (blueIncreasing_) {
            if (b + colorDeltaB_ >= 255) {
                blueIncreasing_ = FALSE;
                borderColor_.b = 255;
                colorDeltaB_ *= -1;
            }
        } else {
            if (b + colorDeltaB_ <= 0) {
                blueIncreasing_ = TRUE;
                borderColor_.b = 0;
                colorDeltaB_ *= -1;
            }
        }
        
        if (greenIncreasing_) {
            if (g + colorDeltaG_ >= 255) {
                greenIncreasing_ = FALSE;
                borderColor_.g = 255;
                colorDeltaG_ *= -1;
            }
        } else {
            if (g + colorDeltaG_ <= 0) {
                greenIncreasing_ = TRUE;
                borderColor_.g = 0;
                colorDeltaG_ *= -1;
            }
        }
        
        borderColor_.r = borderColor_.r + colorDeltaR_;
        borderColor_.b = borderColor_.b + colorDeltaB_;
        borderColor_.g = borderColor_.g + colorDeltaG_;
        
        [vertBorderLeft_ setColor:borderColor_];
        [horzBorderBottom_ setColor:borderColor_];
        [vertBorderRight_  setColor:borderColor_];
        [horzBorderTop_ setColor:borderColor_];
        
        colorTime_ = 0;
    }
    
}

- (void)resetBricks {
    
    CGSize playerLayerSize = [playerLayer_ layerSize];
    for( int x=0; x < playerLayerSize.width;x++) {
        for( int y=0; y < playerLayerSize.height; y++ ) {
            CCSprite *brick = [playerLayer_ tileAt:ccp(x,y)];
            brick.visible = TRUE;
        }
    }
    
    CGSize enemyLayerSize = [enemyLayer_ layerSize];
    for( int x=0; x < enemyLayerSize.width;x++) {
        for( int y=0; y < enemyLayerSize.height; y++ ) {
            CCSprite *brick = [enemyLayer_ tileAt:ccp(x,y)];
            brick.visible = TRUE;
        }
    }
}

- (void)drawBricks {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
 
    playerMap_ = [CCTMXTiledMap tiledMapWithTMXFile:graphicsPath(@"/TileMaps/base01-crab.tmx")];
    playerLayer_ = [playerMap_ layerNamed:@"Layer 0"];
    playerMap_.anchorPoint = ccp(0, 0);
    CGFloat scaleFactor = [[CCDirector sharedDirector] contentScaleFactor];
    
    //Multiply maps width by 0.77 to get real width since using custom octoganol tiles
    if (scaleFactor == 2){
        playerMap_.position = ccp((winSize.width / 2.0) - (playerMap_.contentSize.width / 2.0) * (0.77) + 
        playerLayer_.mapTileSize.width / 4.0, controlBoxHeight_ + 1.0 + playerLayer_.mapTileSize.height / 4.0);
    } else
        playerMap_.position = ccp((winSize.width / 2.0) - (playerMap_.contentSize.width / 2.0) * (0.77) + 
        playerLayer_.mapTileSize.width / 2.0, controlBoxHeight_ + 1.0 + playerLayer_.mapTileSize.height / 2.0);
        
    //playerMap_.isRelativeAnchorPoint = NO;
    [self addChild:playerMap_ z:0];
    
    CGSize playerLayerSize = [playerLayer_ layerSize];
    for( int x=0; x < playerLayerSize.width;x++) {
        for( int y=0; y < playerLayerSize.height; y++ ) {
            CCSprite *brick = [playerLayer_ tileAt:ccp(x,y)];
            brick.anchorPoint = ccp(0.5f, 0.5f);
            [brick setColor:ccc3((y + 1) * 15, 0, 255 - (y + 1) * 15)];
        }
    }
    
    enemyMap_ = [CCTMXTiledMap tiledMapWithTMXFile:graphicsPath(@"/TileMaps/base01-crab-vert-flip.tmx")];
    enemyLayer_ = [enemyMap_ layerNamed:@"Layer 0"];
    enemyMap_.anchorPoint = ccp(0, 0);
    //Multiply maps width by 0.77 to get real width since using octoganol tiles
    if (scaleFactor == 2){
        enemyMap_.position = ccp((winSize.width / 2.0) - (enemyMap_.contentSize.width / 2.0) * (0.77) + 
        enemyLayer_.mapTileSize.width / 4.0, winSize.height - enemyMap_.contentSize.height - controlBoxHeight_ - 
        1.0 + enemyLayer_.mapTileSize.height / 4.0);
    } else 
        enemyMap_.position = ccp((winSize.width / 2.0) - (enemyMap_.contentSize.width / 2.0) * (0.77) + 
        enemyLayer_.mapTileSize.width / 2.0, winSize.height - enemyMap_.contentSize.height - controlBoxHeight_ - 
        1.0 + enemyLayer_.mapTileSize.height / 2.0);
        
    //playerMap_.isRelativeAnchorPoint = NO;
    [self addChild:enemyMap_ z:0];
    
    CGSize enemyLayerSize = [enemyLayer_ layerSize];
    for( int x=0; x < enemyLayerSize.width;x++) {
        for( int y=0; y < enemyLayerSize.height; y++ ) {
            CCSprite *brick = [enemyLayer_ tileAt:ccp(x,y)];
            brick.anchorPoint = ccp(0.5f, 0.5f);
            [brick setColor:ccc3((enemyLayerSize.height - y - 1) * 15, 0, 255 - (enemyLayerSize.height - y - 1) * 15)];
        }
    }

}

-(void)addBlock:(CGPoint)position {
	
	CCSprite *target = [CCSprite spriteWithFile:graphicsPath(@"Block_sml.png") 
										   rect:CGRectMake(0, 0, 20, 8)]; 
	target.position = position;
	[self addChild:target];
}

-(void)resetScores {
    playerScore_ = 0;
    enemyScore_ = 0;
    [playerScoreLabel_ setString:[NSString stringWithFormat:@"%d", playerScore_]];
    [enemyScoreLabel_ setString:[NSString stringWithFormat:@"%d", enemyScore_]];
}

- (void)newGame {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    if (level_ > 3) {
        AILevel_ = level_;
    } else {
        AILevel_ = 3;
    }
        
    // If not attract-mode, then remove game-type menu
    if (!attractMode_) {
        [self removeChild:label_ cleanup:TRUE];
        [self removeChild:label2_ cleanup:TRUE];
        [self removeChild:label3_ cleanup:TRUE];
        [self removeChild:menu_ cleanup:TRUE];
    }
    
    if (singlePlayer_) {
        // Update remaining lives and level
        [playerLivesLabel_ setString:[NSString stringWithFormat:@"Lives:%d", lives_]];
        [levelLabel_ setString:[NSString stringWithFormat:@"Round:%d", level_]];
    }
    
    // Reset self actions
    [self stopAllActions];
    
    // Reset border
    borderColor_ = ccc3(255, 0, 0);
    
    redIncreasing_ = FALSE;
    blueIncreasing_ = TRUE;
    greenIncreasing_ = TRUE;
    colorDeltaR_ = -1;
    colorDeltaB_ = 2;
    colorDeltaG_ = 1;
    
    [vertBorderLeft_ setColor:borderColor_];
    [horzBorderBottom_ setColor:borderColor_];
    [vertBorderRight_  setColor:borderColor_];
    [horzBorderTop_ setColor:borderColor_];
    
    // Reset each ball
    for (Ball *ball in balls_) {
        [ball stopAllActions];
        ball.position = CGPointMake(winSize.width / 2.0, winSize.height / 2.0);
        [ball reset];
        ball.myPlasmaBall.visible = FALSE;
        [ball.myPlasmaBall stopSystem];
        [ball.myPlasmaBall resetSystem];
    }
    
    // Reset ship 
    [self resetGreemo];
    
    // Stop other sounds
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_];
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_ - 1];
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_ - 2];
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_ - 3];

    
    // Reset shields
    [playerShield_ reset];
    [enemyShield_ reset];
    [playerShield_ setTouchPoint1:CGPointMake(winSize.width / 2.0, winSize.height / shieldPosRatio)];
    [enemyShield_ setTouchPoint1:CGPointMake(winSize.width / 2.0, winSize.height - winSize.height / shieldPosRatio)];
    [playerShield_ move];
    [enemyShield_ move];
    
    [self resetBricks];
    
    electrons_.visible = TRUE;
    [electrons_ resetSystem];
    electrons2_.visible = TRUE;
    [electrons2_ resetSystem];
    
    [brickExplosion_ stopSystem];
    brickExplosion_.visible = FALSE;
    [brickExplosion2_ stopSystem];
    brickExplosion2_.visible = FALSE;
    [finalExplosion_ stopSystem];
    finalExplosion_.visible = FALSE;
    [ringExplosion_ stopSystem];
    ringExplosion_.visible = FALSE;

    
    // set current ball speed based on level
    [self setCurrentBallSpeed];
    
    roundOver_ = FALSE;
    gameOver_ = FALSE;
    playerBlown_ = FALSE;
    enemyBlown_ = FALSE;
    
    if (level_ == 1 && lives_ == 2) {
        [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        //[[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:gameResourcePath(@"sound/DownForce07-128-aac.caf")];
    }
    
    // Set up intro voice over
    NSString *voiceString = @"sound/intro-alien-voiceover.mp3";
    id playVoiceEffect = [CCCallFuncO actionWithTarget:self selector:@selector(playSound:) object:(id)voiceString];
    
    // Start ship's initial fly-in sequence
    NSInitialAngle_ = [[NSNumber numberWithFloat:[ball_ randomStart]] retain];
    [ball_ setScalarVelocity:releaseSpeed];
    id wait1second = [CCDelayTime actionWithDuration:1];
    id wait5second = [CCDelayTime actionWithDuration:5];

    if (level_ == 1 && lives_ == 2) {
        
        countDown_ = countTime;
        
        id waitXseconds = [CCDelayTime actionWithDuration:countDown_];
        
        id countdown = [CCCallFuncND actionWithTarget:self selector:@selector(countDown: data:) data:(void *)&countDown_];
        id shipflyin = [CCCallFuncND actionWithTarget:self selector:@selector(shipFlyIn: data:) data:(void *)NSInitialAngle_];
        id pauseReminder = [CCCallFuncN actionWithTarget:self selector:@selector(showPauseReminder)];
        id startBGMusic = [CCCallFuncN actionWithTarget:self selector:@selector(selectNextBGTrack)];
        
        [self runAction:[CCSequence actions:pauseReminder, wait5second, countdown, waitXseconds, startBGMusic, shipflyin, nil]];
    } else {
        id shipflyin = [CCCallFuncND actionWithTarget:self selector:@selector(shipFlyIn: data:) data:(void *)NSInitialAngle_];
        [self runAction:[CCSequence actions:wait1second, shipflyin, nil]];
    }
        
    [self scheduleGreemoFlyIn];
    
    // Play Alien intro voice-over
    if ((!attractMode_ && level_ == 1 && lives_ == 2) || (twoPlayer_ && twoPlayerFirstRound_)) {
        //lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/intro-alien-voiceover.mp3")];
        [self runAction:[CCSequence actions:wait5second, playVoiceEffect, nil]];
    }
}

- (void)scheduleGreemoFlyIn {
    
    // Start ship's fly-in sequence
    
    if (!ball2_.visible) {
        NSInitialAngle2_ = [[NSNumber numberWithFloat:[ball2_ randomStart]] retain];
        [ball2_ setScalarVelocity:releaseSpeed];
    } else {
        // Needed to pass into shipFlyIn to determine random direction ship travels
        float velocityAngle;
        int updown = arc4random() % 100;
        
        if (updown > 49){   // initially ball goes up
            int randRad = (arc4random() % 245) + 35;
            velocityAngle = randRad / 100.00;
        } else {            // initially ball goes down
            int randRad = (arc4random() % 245) + 349;
            velocityAngle = randRad / 100.00;
        }

        NSInitialAngle2_ = [[NSNumber numberWithFloat:velocityAngle] retain];
    }

     
    float shipTimeMod = shipTime;
    if (easy_) {
        shipTimeMod = shipTimeMod * 1.2;
    }
    
    id wait = [CCDelayTime actionWithDuration:shipTimeMod];
    
    if (level_ > 1) wait = [CCDelayTime actionWithDuration:shipTimeMod * 0.75];
    if (level_ > 4) wait = [CCDelayTime actionWithDuration:shipTimeMod * 0.5];
    if (twoPlayer_) wait = [CCDelayTime actionWithDuration:shipTimeMod * 0.75];
    
    id shipflyin = [CCCallFuncND actionWithTarget:self selector:@selector(shipFlyIn: data:) data:(void *)NSInitialAngle2_];
    id callSelf = [CCCallFuncN actionWithTarget:self selector:@selector(scheduleGreemoFlyIn)];
    [self runAction:[CCSequence actions:wait, shipflyin, callSelf, nil]];
}

- (void)resetGreemo {
    CGSize winSize = [[CCDirector sharedDirector] winSize];

    [ship_ stopAllActions];
    ship_.position = CGPointMake(0 - ship_.texture.contentSize.width, winSize.height / 2.0);
    ship_.visible = FALSE;
    alienSpinning_ = FALSE;
    alienSpinningTextureDisplayed_ = FALSE;
    [ship_ setTexture:alienScrunchTexture_];
    [[SimpleAudioEngine sharedEngine] stopEffect:shipsLastSoundID_];
    
    // Stop any previous possible ship sounds
    [[SimpleAudioEngine sharedEngine] stopEffect:shipsLastSoundID_ - 1];
    [[SimpleAudioEngine sharedEngine] stopEffect:shipsLastSoundID_ - 2];
}

- (void)showPauseReminder {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:menu_ cleanup:TRUE];
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    
    // Remind player of pause menu
    NSString *message = @"Tap center arena for options";
    label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label_.scale = 0.0;
    label_.position = ccp(winSize.width / 2.0, winSize.height * 0.5);
    [self addChild:label_ z:5];
    
    id fade = [CCFadeOut actionWithDuration:0.5];
    id scale = [CCScaleTo actionWithDuration:0.5 scale:0.6];
    
    id wait3second = [CCDelayTime actionWithDuration:3.0];
    id removeCountLabel = [CCCallFuncND actionWithTarget:label_ selector:@selector(removeFromParentAndCleanup:) data:(void *)NO];
    [label_ runAction:[CCSequence actions:scale, wait3second, fade, removeCountLabel, nil]];
    
}

- (short)countDown:(CCNode *)node data:(short *)count {
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    id scale = [CCScaleTo actionWithDuration:0.5 scale:1.5];
    id fade = [CCFadeOut actionWithDuration:0.5];
    id wait = [CCDelayTime actionWithDuration:1];

    
    CCLabelBMFont *countLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i", *count] 
                                fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    countLabel.scale = 0.0;
    // Align center
    [countLabel setAnchorPoint: ccp(0.5f, 0.5f)];
    countLabel.position = ccp(winSize.width / 2.0, winSize.height / 2.0);
    [self addChild:countLabel z:5];
    id removeCountLabel = [CCCallFuncND actionWithTarget:countLabel selector:@selector(removeFromParentAndCleanup:) data:(void *)NO];
    [countLabel runAction:[CCSequence actions:scale, fade, removeCountLabel, nil]];
    
    *count = *count - 1;

    if (*count < 1) {
        return *count;
    } else {
        id countdown = [CCCallFuncND actionWithTarget:self selector:@selector(countDown: data:) data:(void *)&countDown_];
        [self runAction:[CCSequence actions:wait, countdown, nil]];
    }
    return *count;
}

- (void) removeThing:(id)sender {
    
	[self removeChild:sender cleanup:NO];

}

- (void)shipFlyIn:(CCNode *)node data:(NSNumber *)NSinitialAngle {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    ccBezierConfig config1;
    ccBezierConfig config2;
    ccBezierConfig config3;
    ccBezierConfig config4;
    
    float initialAngle = [NSinitialAngle floatValue];
    [NSinitialAngle release];
    
    // Left to center Bezier curve control points
    config1.controlPoint_1 = ccp(0 - ship_.texture.contentSize.width, winSize.height / 2.0);
    config1.controlPoint_2 = ccp(winSize.width / 4.0, winSize.height / 2.0 + winSize.height / 12.0);
    config1.endPosition = ccp(winSize.width / 2.0, winSize.height / 2.0);

    // Center to right Bezier curve control points
    config2.controlPoint_1 = ccp(winSize.width / 2.0, winSize.height / 2.0);
    config2.controlPoint_2 = ccp(3.0 * winSize.width / 4.0, winSize.height / 2.0 - winSize.height / 12.0);
    config2.endPosition = ccp(winSize.width + ship_.texture.contentSize.width, winSize.height / 2.0);
    
    // Right to center Bezier curve control points
    config3.controlPoint_1 = ccp(winSize.width + ship_.texture.contentSize.width, winSize.height / 2.0);
    config3.controlPoint_2 = ccp(3.0 * winSize.width / 4.0, winSize.height / 2.0 - winSize.height / 12.0);
    config3.endPosition = ccp(winSize.width / 2.0, winSize.height / 2.0);
 
    // Center to left Bezier curve control points
    config4.controlPoint_1 = ccp(winSize.width / 2.0, winSize.height / 2.0);
    config4.controlPoint_2 = ccp(winSize.width / 4.0, winSize.height / 2.0 + winSize.height / 12.0);
    config4.endPosition = ccp(0 - ship_.texture.contentSize.width, winSize.height / 2.0);
    
    // Movement actions
    id shipLeftToCentre = [CCBezierTo actionWithDuration:5 bezier:config1];
    id shipCentreToRight = [CCBezierTo actionWithDuration:4 bezier:config2];
    id shipRightToCentre = [CCBezierTo actionWithDuration:5 bezier:config3];
    id shipCentreToLeft = [CCBezierTo actionWithDuration:4 bezier:config4];
    
    // Calculate angle ship will rotate through before ball launch
    float finalAngle = (((arc4random() % 2) + 1) * SJ_PI_X_2 + initialAngle) / M_PI * 180.0; // In degrees
    // Negate because Cocos is stupid and angles increase clockwise by default
    finalAngle = finalAngle * -1.0;
    
    //id wait1second = [CCDelayTime actionWithDuration:1];
    
    NSString *flyInString = @"sound/ship-fly-in-160.mp3"; 
    NSString *flyOutString = @"sound/ship-fly-out-160.mp3"; 
    NSString *cruisingString = @"sound/ship-cruising-160.mp3"; 

    // Sound effect actions
    id playFlyInEffect = [CCCallFuncO actionWithTarget:self selector:@selector(playSound:) object:(id)flyInString];
    id playFlyOutEffect = [CCCallFuncO actionWithTarget:self selector:@selector(playSound:) object:(id)flyOutString];
    id playCruisingEffect = [CCCallFuncO actionWithTarget:self selector:@selector(playSound:) object:(id)cruisingString];
    id stopLastEffect = [CCCallFuncND actionWithTarget:self selector:@selector(stopSound:data:) data:(void *)&shipsLastSoundID_];
    id ballVisible;
    id ballActive;
    id plasmoidVisible;
    id shipVisible = [CCCallFuncND actionWithTarget:self selector:@selector(setVisible: data:) data:ship_];
    id shipInvisible = [CCCallFuncND actionWithTarget:self selector:@selector(setInvisible: data:) data:ship_];
    id setSpinning = [CCCallFuncND actionWithTarget:self selector:@selector(setSpinning: data:) data:ship_];

    ballVisible = [CCCallFuncND actionWithTarget:self selector:@selector(setVisible: data:) data:ball_];
    ballActive = [CCCallFuncND actionWithTarget:self selector:@selector(scheduleBallActive: data:) data:ball_];
    //[plasmaBall_ resetSystem];
    plasmoidVisible = [CCCallFuncND actionWithTarget:self selector:@selector(setVisible: data:) data:plasmaBall_];
     
    float rotationReturnTime;  // Time it takes for ship to rotate after releasing ball
    
    
    // If ball is launching upwards then ship enters from the right
    if (initialAngle >= 0.0 && initialAngle <= M_PI) {
        // Subtract another 180 degrees because this sprite is rotated by default
        finalAngle -= 180.0;
        float rotationTime = fabsf(finalAngle) / (shipRotateSpeed);  // Angle divided by Degrees per second
        id shipRandRotate = [CCRotateBy actionWithDuration:rotationTime angle:finalAngle];
        rotationReturnTime = fabsf(initialAngle - M_PI) / (shipRotateSpeed / 180.0 * M_PI);
        ship_.position = ccp(winSize.width + ship_.texture.contentSize.width * 2, winSize.height / 2.0);
        ship_.rotation = 180.0;
        id shipRotateLeft = [CCRotateTo actionWithDuration:rotationReturnTime angle:180.0];
        if (!ball_.visible) {
            [ship_ runAction:[CCSequence actions:playFlyInEffect, shipVisible, shipRightToCentre, stopLastEffect, playCruisingEffect, setSpinning, plasmoidVisible, shipRandRotate,
                              ballVisible, shipRotateLeft, ballActive, setSpinning, stopLastEffect, playFlyOutEffect, shipCentreToLeft, shipInvisible, nil]];
        } else {
            [ship_ runAction:[CCSequence actions:playFlyInEffect, shipVisible, shipRightToCentre, stopLastEffect, playFlyOutEffect, shipCentreToLeft, shipInvisible, nil]];
        }
        
        
    // If ball is launching downwards then ship enters from the left    
    } else { 
        float rotationTime = fabsf(finalAngle) / (shipRotateSpeed);  // Angle divided by Degrees per second
        id shipRandRotate = [CCRotateBy actionWithDuration:rotationTime angle:finalAngle];
        rotationReturnTime = fabsf(initialAngle - SJ_PI_X_2) / (shipRotateSpeed / 180.0 * M_PI);
        ship_.position = ccp(0 - ship_.texture.contentSize.width * 2, winSize.height / 2.0);
        ship_.rotation = 0.0;
        id shipRotateRight = [CCRotateTo actionWithDuration:rotationReturnTime angle:0.0];
        if (!ball_.visible) {
            [ship_ runAction:[CCSequence actions:playFlyInEffect, shipVisible, shipLeftToCentre, stopLastEffect, playCruisingEffect, setSpinning, plasmoidVisible, shipRandRotate,
                              ballVisible, shipRotateRight, ballActive, setSpinning, stopLastEffect, playFlyOutEffect, shipCentreToRight, shipInvisible, nil]];
        } else {
            [ship_ runAction:[CCSequence actions:playFlyInEffect, shipVisible, shipLeftToCentre, stopLastEffect, playFlyOutEffect, shipCentreToRight, shipInvisible, nil]];
        }
        
    }
}

- (void)setVisible:(id)nothing data:(CCNode *)node {
    node.visible = TRUE;
    //NSLog(@"Node set visible");
}

- (void)setInvisible:(id)nothing data:(CCNode *)node {
    node.visible = FALSE;
    //NSLog(@"Node set visible");
}

- (void)setSpinning:(id)nothing data:(CCNode *)node {
    if (alienSpinning_) {
        alienSpinning_ = FALSE;
    } else {
        alienSpinning_ = TRUE;
    }
}

- (void)scheduleBallActive:(id)nothing data:(Ball *)ball {
    
    id actionDelay = [CCDelayTime actionWithDuration:0.25];
	id actionDone = [CCCallFuncND actionWithTarget:self selector:@selector(setBallActive: data:) data:ball];
	[self runAction:[CCSequence actions:actionDelay, actionDone, nil]];
        
}

- (void)setBallActive:(id)nothing data:(Ball *)ball {
    ball.active = TRUE;
    //NSLog(@"Ball set active");
}

- (void)playSound:(NSString *)string {
    shipsLastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(string)];
    lastSoundID_ = shipsLastSoundID_;
}

- (void)stopSound:(CCNode *)node data:(ALuint *)ident {
    [[SimpleAudioEngine sharedEngine] stopEffect:*ident];
}
                                                                           
-(void)selectNextBGTrack {
    switch (nextTrack_) {
        case 0:
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/DownForce07-128-aac.caf") loop:NO];
            nextTrack_ = 1;
            break;
            
        case 1:
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/ComeCloser-128-aac.caf") loop:NO];
            nextTrack_ = 2;
            break;
            
        case 2:
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/CloudsStars2-128-aac.caf") loop:NO];
            nextTrack_ = 3;
            break;
            
        case 3:
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/Monolith-128-aac.caf") loop:NO];
            nextTrack_ = 4;
            break;
            
        case 4:
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/DontLookBack-128-aac.caf") loop:NO];
            nextTrack_ = 5;
            break;
            
        case 5:
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:gameResourcePath(@"sound/Synchronicity-128-aac.caf") loop:NO];
            nextTrack_ = 0;
            break;
            
        default:
            break;
    }
    if (musicPaused_) {
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
}

- (void)endScene:(id)sender {
    
    [self removeChild:menu_ cleanup:TRUE];
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    
    //if (gameOver_) return;
    roundOver_ = true;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    NSString *message;
    NSString *message3;

    CCLabelBMFont *nextRoundLabel;
    
    CCLabelBMFont *restartLabel;
    restartLabel = [CCLabelBMFont labelWithString:@"Reset Game" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCMenuItemLabel *restartItem = [CCMenuItemLabel itemWithLabel:restartLabel target:self selector:@selector(verifyReset:)];
    restartItem.scale = 0.0;
    restartItem.position = ccp(winSize.width/2, winSize.height * 0.4);
    
    CCLabelBMFont *achievementLabelText = [CCLabelBMFont labelWithString:@"" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    if (singlePlayer_) {
        if (easy_) {
            if (playerScore_ > [GameState sharedInstance].highScoreEasy) {
                [GameState sharedInstance].highScoreEasy = playerScore_;
                newHighScore_ = TRUE;
            }
            [[GameState sharedInstance] save];
            [[GCHelper sharedInstance] reportScore:kLeaderboardOverlordsHS1 score:(unsigned int)playerScore_];
            
        } else {
            if (playerScore_ > [GameState sharedInstance].highScoreHard) {
                [GameState sharedInstance].highScoreHard = playerScore_;
                newHighScore_ = TRUE;
            }
            [[GameState sharedInstance] save];
            [[GCHelper sharedInstance] reportScore:kLeaderboardOverlordsHS101 score:(unsigned int)playerScore_];
        }
        if (newHighScore_) {
            message3 = @"NEW HIGH SCORE!";
            label3_ = [CCLabelBMFont labelWithString:message3 fntFile:graphicsPath(@"Fonts/OCR.fnt")];
            label3_.scale = 0.0;
            label3_.position = ccp(winSize.width/2, winSize.height * 0.8);
            [self addChild:label3_ z:5];
        }
    }
    
    if (gameOver_ && singlePlayer_) {
        message = @"Die Humon SCUM!";
        label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        label_.scale = 0.0;
        label_.position = ccp(winSize.width/2, winSize.height * 0.6);
        [self addChild:label_ z:5];
        menu_ = [CCMenu menuWithItems:restartItem, nil];
        menu_.position = CGPointZero;
        [self addChild:menu_ z:5];
        [restartItem runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
        [label_ runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
        
        lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/intro-alien-voiceover.mp3")];
        
        return;
    }
    
    if (twoPlayer_) {
        twoPlayerFirstRound_ = FALSE;
        nextRoundLabel = [CCLabelBMFont labelWithString:@"Next Round?" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
        message = @"Excellent, Humon!";
        [Appirater userDidSignificantEvent:YES];
        [self schedule:@selector(playNiceMoveEffect) interval:2];
        
    } else if (singlePlayer_ && !gameOver_ && endReason_ == kEndReasonWin) {
        [Appirater userDidSignificantEvent:YES];
        nextRoundLabel = [CCLabelBMFont labelWithString:@"Next Round?" fntFile:graphicsPath(@"Fonts/OCR.fnt")]; 
        message = @"Excellent, Humon!";
        [self schedule:@selector(playNiceMoveEffect) interval:2];
        
        if (level_ == 2) {
            CCLOG(@"Finished level 1");
            //if (![GameState sharedInstance].completedLevel1) {
                [GameState sharedInstance].completedLevel1 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel1
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Humon Shield!";
            //}
        } else if (level_ == 6) {
            CCLOG(@"Finished level 5");
            //if (![GameState sharedInstance].completedLevel5) {
                [GameState sharedInstance].completedLevel5 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel5
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Exsanguinator!";
            //}
        } else if (level_ == 11) {
            CCLOG(@"Finished level 10");
            //if (![GameState sharedInstance].completedLevel10) {
                [GameState sharedInstance].completedLevel10 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel10
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Berserker!";
            //}
        } else if (level_ == 26) {
            CCLOG(@"Finished level 25");
            //if (![GameState sharedInstance].completedLevel25) {
                [GameState sharedInstance].completedLevel25 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel25
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Galactic Antaeus!";
            //}
        } else if (level_ == 51) {
            CCLOG(@"Finished level 50");
            //if (![GameState sharedInstance].completedLevel50) {
                [GameState sharedInstance].completedLevel50 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel50
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Galactic Hercules!";
            //}
        } else if (level_ == 101) {
            CCLOG(@"Finished level 100");
            //if (![GameState sharedInstance].completedLevel100) {
                [GameState sharedInstance].completedLevel100 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel100
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Overlord!";
            //}
        } else if (level_ == 1001) {
            CCLOG(@"Finished level 1000");
            //if (![GameState sharedInstance].completedLevel1000) {
                [GameState sharedInstance].completedLevel1000 = true;
                [[GameState sharedInstance] save];
                [[GCHelper sharedInstance] reportAchievement:kAchievementLevel1000
                                             percentComplete:100.0];
                achievementLabelText.string = @"New Title: Space Anomaly!";
            //}
        } 
        
    } else if (singlePlayer_ && !gameOver_ && endReason_ == kEndReasonLose) {
        nextRoundLabel = [CCLabelBMFont labelWithString:@"Resurect?" fntFile:graphicsPath(@"Fonts/OCR.fnt")]; 
        message = @"FAILURE HUMON!";
    }
    
    label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label_.scale = 0.0;
    label_.position = ccp(winSize.width/2, winSize.height * 0.6);
    [self addChild:label_ z:5];
    
    label2_ = achievementLabelText;
    label2_.scale = 0.0;
    [label2_ setPosition:ccp(winSize.width / 2.0, winSize.height * 0.7f)];
    [self addChild:label2_ z:5];
    
    CCMenuItemLabel *nextRoundItem = [CCMenuItemLabel itemWithLabel:nextRoundLabel target:self selector:@selector(nextRound:)];
    nextRoundItem.scale = 0.0;
    nextRoundItem.position = ccp(winSize.width/2, winSize.height * 0.5);
    
    menu_ = [CCMenu menuWithItems:restartItem, nextRoundItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    //[nextRoundItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [restartItem runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    [label_ runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [label2_ runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    [label3_ runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.1];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    
    [nextRoundItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
}

- (void)playNiceMoveEffect {
    lastSoundID_ = [[SimpleAudioEngine sharedEngine] playEffect:gameResourcePath(@"sound/nice-move-voiceover.mp3")];
    [self unschedule:@selector(playNiceMoveEffect)];
}

- (void)restartTapped:(id)sender {
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_];
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_ - 1];
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_ - 2];
    [[SimpleAudioEngine sharedEngine] stopEffect:lastSoundID_ - 3];

    [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:[OverlordsScene node]]];   
}

- (void)verifyReset:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:menu_ cleanup:TRUE];
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    
    // Ask for verification
    NSString *message = @"Are you sure?";
    label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label_.scale = 0.0;
    label_.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    [self addChild:label_ z:5];
    
    CCLabelBMFont *yesLabel;
    yesLabel = [CCLabelBMFont labelWithString:@"YES" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCLabelBMFont *noLabel;
    noLabel = [CCLabelBMFont labelWithString:@"NO" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCMenuItemLabel *yesItem = [CCMenuItemLabel itemWithLabel:yesLabel target:self selector:@selector(restartTapped:)];
    yesItem.scale = 0.0;
    yesItem.position = ccp(winSize.width / 3.0, winSize.height * 0.5);
    
    CCMenuItemLabel *noItem = [CCMenuItemLabel itemWithLabel:noLabel target:self selector:@selector(endScene:)];
    noItem.scale = 0.0;
    noItem.position = ccp(2.0 * winSize.width / 3.0, winSize.height * 0.5);
    
    menu_ = [CCMenu menuWithItems:yesItem, noItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    [label_ runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.1];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    id scaleUp2 = [CCScaleTo actionWithDuration:0.5 scale:1.1];
    id scaleNorm2 = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    
    [yesItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    [noItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp2, scaleNorm2, nil]]];
}

- (void)onePlayerTapped:(id)sender {
    singlePlayer_ = TRUE;
    twoPlayer_ = FALSE;
    [self difficultySelect:self];
}

- (void)twoPlayerTapped:(id)sender {
    singlePlayer_ = FALSE;
    twoPlayer_ = TRUE;
    [self difficultySelect:self];
}

- (void)setEasy:(id)sender {
    easy_ = TRUE;
    [self gameStart:self];
}

- (void)gameStart:(id)sender {
    
    if (singlePlayer_) {
        [self onePlayerStart];
    } else {
        [self twoPlayerStart];
    }
    [GameState sharedInstance].easy = easy_;
    [[GameState sharedInstance] save];
}

- (void)onePlayerStart {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    newGame_ = TRUE;
    singlePlayer_ = TRUE;
    twoPlayer_ = FALSE;
    [enemyShield_ setAiPlayer:TRUE];
    [playerShield_ setAiPlayer:FALSE];
    [self resetScores];
    attractMode_ = FALSE;
    level_ = startLevel;
    
    // Show Player Lives Remaining
    NSString *playerLives = [NSString stringWithFormat:@"Lives:%d", lives_];
    playerLivesLabel_ = [CCLabelBMFont labelWithString:playerLives fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    playerLivesLabel_.scale = 0.5;
    // Align left
    [playerLivesLabel_ setAnchorPoint: ccp(0.0f, 0.5f)];
    playerLivesLabel_.position = ccp(winSize.width/2.0 - winSize.height/3.0 + winSize.height / 96.0, controlBoxHeight_ / 2.0 
                                     - playerLivesLabel_.contentSize.height / 10.0);
    [self addChild:playerLivesLabel_ z:5];
    
    // Show game round
    NSString *level = [NSString stringWithFormat:@"Round:%d", level_];
    levelLabel_ = [CCLabelBMFont labelWithString:level fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    levelLabel_.scale = 0.5;
    // Align right
    [levelLabel_ setAnchorPoint: ccp(1.0f, 0.5f)];
    levelLabel_.position = ccp(winSize.width / 2.0 + winSize.height / 3.0 - winSize.height / 96.0, winSize.height - controlBoxHeight_ / 2.0 
                               - playerScoreLabel_.contentSize.height / 10.0);
    [self addChild:levelLabel_ z:5];
    
    // Rotate enemy score so player can read it
    [enemyScoreLabel_ setAnchorPoint: ccp(0.0f, 0.5f)];
    [enemyScoreLabel_ setRotation:0];
    enemyScoreLabel_.position = ccp(winSize.width/2.0 - winSize.height/3.0 + winSize.height / 96.0, winSize.height - controlBoxHeight_ / 2.0 
                                    - enemyScoreLabel_.contentSize.height / 10.0);
    
    // Rotate enemy magnetron power level label
    [enemyMagLvlLabel_ setAnchorPoint: ccp(0.5f, 0.5f)];
    [enemyMagLvlLabel_ setRotation:0];
    enemyMagLvlLabel_.position = ccp(winSize.width/2.0, winSize.height - controlBoxHeight_ / 2.0 
                                    - enemyMagLvlLabel_.contentSize.height / 10.0);
    [self newGame];
}

- (void)twoPlayerStart {
    newGame_ = TRUE;
    singlePlayer_ = FALSE;
    twoPlayer_ = TRUE;
    twoPlayerFirstRound_ = TRUE;
    attractMode_ = FALSE;
    [playerShield_ setAiPlayer:FALSE];
    [enemyShield_ setAiPlayer:FALSE];
    [self resetScores];
    level_ = startLevel;
    [self newGame];
}

-(void)nextRound:(id)sender {
    [self newGame];
}

- (void)showMainMenu:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
    
    //Show 1 player and 2 player buttons
    NSString *message = @"PRESS 1 OR 2 PLAYER START";
    label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label_.scale = 0.0;
    label_.position = ccp(winSize.width / 2.0, winSize.height * 0.58);
    [self addChild:label_ z:5];
    
    CCLabelBMFont *onePlayerLabel;
    onePlayerLabel = [CCLabelBMFont labelWithString:@"1P" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCLabelBMFont *twoPlayerLabel;
    twoPlayerLabel = [CCLabelBMFont labelWithString:@"2P" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCLabelBMFont *optionsLabel;
    optionsLabel = [CCLabelBMFont labelWithString:@"Options" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCMenuItemLabel *onePlayerItem = [CCMenuItemLabel itemWithLabel:onePlayerLabel target:self selector:@selector(onePlayerTapped:)];
    onePlayerItem.scale = 0.0;
    onePlayerItem.position = ccp(winSize.width / 3.0, winSize.height * 0.48);
    
    CCMenuItemLabel *twoPlayerItem = [CCMenuItemLabel itemWithLabel:twoPlayerLabel target:self selector:@selector(twoPlayerTapped:)];
    twoPlayerItem.scale = 0.0;
    twoPlayerItem.position = ccp(2.0 * winSize.width / 3.0, winSize.height * 0.48);
    
    CCMenuItemLabel *optionsItem = [CCMenuItemLabel itemWithLabel:optionsLabel target:self selector:@selector(optionsTapped:)];
    optionsItem.scale = 0.0;
    optionsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.4);
    
    menu_ = [CCMenu menuWithItems:onePlayerItem, twoPlayerItem, optionsItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.2];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    id scaleUp2 = [CCScaleTo actionWithDuration:0.5 scale:1.2];
    id scaleNorm2 = [CCScaleTo actionWithDuration:0.5 scale:1.0];

    [optionsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    [label_ runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    
    [onePlayerItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    [twoPlayerItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp2, scaleNorm2, nil]]];
}

- (void)difficultySelect:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
    
    CCLabelBMFont *easyLabel;
    easyLabel = [CCLabelBMFont labelWithString:@"Easy" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *easyItem = [CCMenuItemLabel itemWithLabel:easyLabel target:self selector:@selector(setEasy:)];
    easyItem.scale = 0.0;
    easyItem.position = ccp(winSize.width / 2.0, winSize.height * 0.57);
    
    CCLabelBMFont *hardLabel;
    hardLabel = [CCLabelBMFont labelWithString:@"Hard" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *hardItem = [CCMenuItemLabel itemWithLabel:hardLabel target:self selector:@selector(gameStart:)];
    hardItem.scale = 0.0;
    hardItem.position = ccp(winSize.width / 2.0, winSize.height * 0.42);
 
    
    menu_ = [CCMenu menuWithItems:easyItem, hardItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.2];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    id scaleUp2 = [CCScaleTo actionWithDuration:0.5 scale:1.2];
    id scaleNorm2 = [CCScaleTo actionWithDuration:0.5 scale:1.0];
     
    [easyItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    [hardItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp2, scaleNorm2, nil]]];    
}

- (void)optionsTapped:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
        
    CCLabelBMFont *mainMenuLabel;
    mainMenuLabel = [CCLabelBMFont labelWithString:@"Main Menu" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    

    CCMenuItemLabel *mainMenuItem = [CCMenuItemLabel itemWithLabel:mainMenuLabel target:self selector:@selector(showMainMenu:)];
    mainMenuItem.scale = 0.0;
    mainMenuItem.position = ccp(winSize.width / 2.0, winSize.height * 0.7);
    
    CCLabelBMFont *leaderboardLabel;
    leaderboardLabel = [CCLabelBMFont labelWithString:@"High Scores" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *leaderboardItem = [CCMenuItemLabel itemWithLabel:leaderboardLabel target:self selector:@selector(showLeaderboard:)];
    leaderboardItem.scale = 0.0;
    leaderboardItem.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    
    CCLabelBMFont *achievementsLabel;
    achievementsLabel = [CCLabelBMFont labelWithString:@"Achievements" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *achievementsItem = [CCMenuItemLabel itemWithLabel:achievementsLabel target:self selector:@selector(showAchievements:)];
    achievementsItem.scale = 0.0;
    achievementsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.5);
    
    CCLabelBMFont *creditsLabel;
    creditsLabel = [CCLabelBMFont labelWithString:@"Credits" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    
    CCMenuItemLabel *creditsItem = [CCMenuItemLabel itemWithLabel:creditsLabel target:self selector:@selector(creditsTapped:)];
    creditsItem.scale = 0.0;
    creditsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.4);
    
    CCLabelBMFont *soundLabel;
    if ([[SimpleAudioEngine sharedEngine] mute]) {
        soundLabel = [CCLabelBMFont labelWithString:@"Sound on" fntFile:graphicsPath(@"Fonts/OCR.fnt")];    
    } else {
        soundLabel = [CCLabelBMFont labelWithString:@"Sound off" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    }
    
    CCMenuItemLabel *soundItem = [CCMenuItemLabel itemWithLabel:soundLabel target:self selector:@selector(soundOffTapped:)];
    soundItem.scale = 0.0;
    soundItem.position = ccp(winSize.width / 2.0, winSize.height * 0.3);
    
    CCLabelBMFont *musicLabel;
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]) {
        musicLabel = [CCLabelBMFont labelWithString:@"Music on" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    } else {
        musicLabel = [CCLabelBMFont labelWithString:@"Music off" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    }
    
    CCMenuItemLabel *musicItem = [CCMenuItemLabel itemWithLabel:musicLabel target:self selector:@selector(musicOffTapped:)];
    musicItem.scale = 0.0;
    musicItem.position = ccp(winSize.width / 2.0, winSize.height * 0.2);

    menu_ = [CCMenu menuWithItems:creditsItem, leaderboardItem, achievementsItem, soundItem, musicItem, mainMenuItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    [creditsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [soundItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [musicItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [mainMenuItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [leaderboardItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
    [achievementsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];

}

- (void)showLeaderboard:(id)sender {
    
    if ([[GCHelper sharedInstance] isGameCenterAvailable]) { // Only if Game Center is available
        CCLOG(@"Show Leaderboard!");
        GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
        if (leaderboardController != NULL)
        {
            if ([GameState sharedInstance].easy == TRUE) {
                leaderboardController.category = kLeaderboardOverlordsHS1;
            } else {
                leaderboardController.category = kLeaderboardOverlordsHS101;
            }
            leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
            AppDelegate *delegate = [UIApplication sharedApplication].delegate;
            leaderboardController.leaderboardDelegate = self;
            [delegate.viewController presentModalViewController:leaderboardController animated:YES];
        }
    }
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.viewController dismissModalViewControllerAnimated: YES];
    [viewController release];
}

- (void)showAchievements:(id)sender {
    
    if ([[GCHelper sharedInstance] isGameCenterAvailable]) { // Only if Game Center is available
        CCLOG(@"Show achievements!");
        AppDelegate *delegate = [UIApplication sharedApplication].delegate;
        GKAchievementViewController *achievements = [[GKAchievementViewController alloc] init];
        if (achievements != NULL) {
            achievements.achievementDelegate = self;
            [delegate.viewController presentModalViewController: achievements animated: YES];
        }
    }
}

- (void)achievementViewControllerDidFinish:(GKAchievementViewController *)viewController {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    [delegate.viewController dismissModalViewControllerAnimated: YES];
    [viewController release];
}

- (void)creditsTapped:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
    
    NSString *message = @"(c) 2012 HyperGalactic Games Corp.\nVisit: www.hypergalactic.ca\n\nDesign: Robert Wasmann\n        Dr. Lisa Glass\nProgramming: Robert Wasmann\nArt, Sound:  Robert Wasmann\nMath Wizardry: Dr. Lisa Glass\n\nMusic by BEN (1H1D!!!)\n1H1D!!! Official Site:\nhttp://www.1h1d.net/\n1H1D!!! on Bandcamp:\nhttp://1h1d.bandcamp.com/\n\nSpecial Thanks: You!";
    
    label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label_.scale = 0.0;
    label_.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    [self addChild:label_ z:5];
    
    // get the app's version and display it
	NSString *version = [NSString stringWithFormat:@"v%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString*)kCFBundleVersionKey]];
    label2_ = [CCLabelBMFont labelWithString:version fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label2_.scale = 0.0;
    // Align left
    [label2_ setAnchorPoint: ccp(0.0f, 0.5f)];
    label2_.position = ccp(winSize.width/2.0 - winSize.height/3.0 + winSize.height / 96.0, controlBoxHeight_ / 2.0
                           - label2_.contentSize.height / 10.0);
    [self addChild:label2_ z:5];
    
    CCLabelBMFont *optionsLabel;
    optionsLabel = [CCLabelBMFont labelWithString:@"Options" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *optionsItem;
    optionsItem = [CCMenuItemLabel itemWithLabel:optionsLabel target:self selector:@selector(optionsTapped:)];
    
    optionsItem.scale = 0.0;
    optionsItem.position = ccp(winSize.width / 2.0, winSize.height * 0.25);
    
    menu_ = [CCMenu menuWithItems:optionsItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    //[optionsItem runAction:[CCScaleTo actionWithDuration:0.5 scale:0.7]];
    [label_ runAction:[CCScaleTo actionWithDuration:0.5 scale:0.5]];
    [label2_ runAction:[CCScaleTo actionWithDuration:0.5 scale:0.5]];

    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:0.77];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:0.7];
    
    [optionsItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    
}

- (void)musicError:(id)sender {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
    
    NSString *message = @"Sorry, but you must stop the\nexternal audio player first!";
    
    label_ = [CCLabelBMFont labelWithString:message fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    label_.scale = 0.0;
    label_.position = ccp(winSize.width / 2.0, winSize.height * 0.6);
    [self addChild:label_ z:5];
    
    CCLabelBMFont *okLabel;
    okLabel = [CCLabelBMFont labelWithString:@"OK" fntFile:graphicsPath(@"Fonts/OCR.fnt")];
    
    CCMenuItemLabel *okItem;
    okItem = [CCMenuItemLabel itemWithLabel:okLabel target:self selector:@selector(optionsTapped:)];
    
    okItem.scale = 0.0;
    okItem.position = ccp(winSize.width / 2.0, winSize.height * 0.45);
    
    menu_ = [CCMenu menuWithItems:okItem, nil];
    menu_.position = CGPointZero;
    [self addChild:menu_ z:5];
    
    [label_ runAction:[CCScaleTo actionWithDuration:0.5 scale:0.6]];
    
    id scaleUp = [CCScaleTo actionWithDuration:0.5 scale:1.2];
    id scaleNorm = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    
    [okItem runAction:[CCRepeatForever actionWithAction:[CCSequence actions:scaleUp, scaleNorm, nil]]];
    
}


- (void)soundOffTapped:(id)sender {
    if ([[SimpleAudioEngine sharedEngine] mute]) {
        [[SimpleAudioEngine sharedEngine] setMute:FALSE];
    } else {
        [[SimpleAudioEngine sharedEngine] setMute:TRUE];
    }
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
    
    if (attractMode_) {
        [self optionsTapped:self];
    }  
}

- (void)musicOffTapped:(id)sender {
    if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]) {
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
        musicPaused_ = YES;
        
    } else {
        
        // If ipod music is playing, display an error advising to shut it off first
        if ([[CDAudioManager sharedManager] isOtherAudioPlaying] && attractMode_) {
            [self musicError:self];
            return;
        }
        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
        musicPaused_ = NO;
    }
    [AccessDefaults defaultSetMusicPaused:musicPaused_];
    
    [self removeChild:label_ cleanup:TRUE];
    [self removeChild:label2_ cleanup:TRUE];
    [self removeChild:label3_ cleanup:TRUE];
    [self removeChild:menu_ cleanup:TRUE];
    
    if (attractMode_) {
        [self optionsTapped:self];
    }
}

-(void)doPause {
	ccColor4B c ={0,0,0,100};
	[PauseLayer layerWithColor:c delegate:self];
}

-(void)pauseLayerDidPause {
    gamePaused_ = TRUE;
}

-(void)pauseLayerDidUnpause {
    self.isTouchEnabled = NO;
    gamePaused_ = FALSE;
    [playerShield_ wasPaused];
    [enemyShield_ wasPaused];
}

- (void)setCurrentBallSpeed {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    // Define initial and max speeds keeping consistant between iPhone and iPad

    float initSlowBallSpeed = _initSlowBallSpeed_ * winSize.height / 480.0; // Minimum ball speed
    float maxSlowBallSpeed = _maxSlowBallSpeed_ * winSize.height / 480.0; // Minimum ball speed

    float initFastBallSpeed = _initFastBallSpeed_ * winSize.height / 480.0; // Maximum ball speed
    float maxFastBallSpeed = _maxFastBallSpeed_ * winSize.height / 480.0; // Maximum ball speed

    float initHyperBallSpeed = _initHyperBallSpeed_ * winSize.height / 480.0; // Max speed attainable by human flick
    float maxHyperBallSpeed = _maxHyperBallSpeed_ * winSize.height / 480.0; // Max speed attainable by human flick

    // Set current speeds determined by current level
    slowBallSpeed_ = ((maxSlowBallSpeed - initSlowBallSpeed)/log(peakLevel)) * log(AILevel_) + initSlowBallSpeed;
    fastBallSpeed_ = ((maxFastBallSpeed - initFastBallSpeed)/log(peakLevel)) * log(AILevel_) + initFastBallSpeed;
    hyperBallSpeed_ = ((maxHyperBallSpeed - initHyperBallSpeed)/log(peakLevel)) * log(AILevel_) + initHyperBallSpeed;
    
    if (easy_) {
        slowBallSpeed_ = slowBallSpeed_ * 0.75;
        fastBallSpeed_ = fastBallSpeed_ * 0.75;
        hyperBallSpeed_ = hyperBallSpeed_ * 0.75;
    }

}

-(void)draw {
/*		CGSize winSize = [[CCDirector sharedDirector] winSize];
 		// calculate the scaled height for the control area
 		float controlHeight = controllerHeight / 480 * winSize.height; 
	
		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		glLineWidth(1.0f);	
		
		//Left vetical boundary line points
		CGPoint lpt1 = CGPointMake(winSize.width/2.0 - winSize.height/3.0 + 1.0, 1.0);
		CGPoint lpt2 = CGPointMake(winSize.width/2.0 - winSize.height/3.0 + 1.0, winSize.height - controlHeight - 1.0);
		//right vertical boundary line points
		CGPoint lpt3 = CGPointMake(winSize.width/2.0 + winSize.height/3.0 - 1.0, 1.0);
		CGPoint lpt4 = CGPointMake(winSize.width/2.0 + winSize.height/3.0 - 1.0, winSize.height - controlHeight - 1.0);
		//bottom horizontal boundary line points
		CGPoint lpt5 = CGPointMake(winSize.width/2.0 - winSize.height/3.0 + 1.0, 1.0);
		CGPoint lpt6 = CGPointMake(winSize.width/2.0 + winSize.height/3.0 - 1.0, 1.0);
		//top horizontal boundary line points ( the - 0.5 is due to rounding errors)
		CGPoint lpt7 = CGPointMake(winSize.width/2.0 - winSize.height/3.0 + 1.0, winSize.height - controlHeight - 1.0);
		CGPoint lpt8 = CGPointMake(winSize.width/2.0 + winSize.height/3.0 - 0.5, winSize.height - controlHeight - 1.0);
		
		ccDrawLine(lpt1, lpt2);
		ccDrawLine(lpt3, lpt4);
		ccDrawLine(lpt5, lpt6);
		ccDrawLine(lpt7, lpt8);
		//CGPoint points[8]={lpt1,lpt2,lpt3,lpt4,lpt5,lpt6,lpt7,lpt8};
		//[self ccDrawLines:points:8]; */
}


//example usage
//CGPoint points[8]={{10,20},{30,20},{30,40},{10,40},{80,80},{100,80},{100,100},{80,100}};
//ccDrawLines(points,8);

-(void)ccDrawLines:(CGPoint*)points:(uint)numberOfPoints  {
	//layout of points [0] = origin, [1] = destination and so on
	
	ccVertex2F vertices[numberOfPoints];
	if (CC_CONTENT_SCALE_FACTOR() != 1 )
	{
		for (int i=0;i<numberOfPoints;i++)
		{
			vertices[i].x=points[i].x * CC_CONTENT_SCALE_FACTOR();
			vertices[i].y=points[i].y * CC_CONTENT_SCALE_FACTOR();
		}
		glVertexPointer(2, GL_FLOAT, 0, vertices);
	}
	else glVertexPointer(2, GL_FLOAT, 0, points);
	// Default GL states: GL_TEXTURE_2D, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_TEXTURE_COORD_ARRAY
	// Needed states: GL_VERTEX_ARRAY,
	// Unneeded states: GL_TEXTURE_2D, GL_TEXTURE_COORD_ARRAY, GL_COLOR_ARRAY
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	
	glDrawArrays(GL_LINES, 0, numberOfPoints);
	
	// restore default state
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);
}

-(void)addRedBorder {

	CGSize winSize = [[CCDirector sharedDirector] winSize];
	// calculate the scaled height for the control area
	controlBoxHeight_ = controlBoxRatio * winSize.height; 
	
	vertBorderLeft_ = [CCSprite spriteWithFile:graphicsPath(@"vert-line.png")];
	horzBorderBottom_ = [CCSprite spriteWithFile:graphicsPath(@"horiz-line.png")];
	vertBorderRight_ = [CCSprite spriteWithFile:graphicsPath(@"vert-line.png")];
	horzBorderTop_ = [CCSprite spriteWithFile:graphicsPath(@"horiz-line.png")];
    
    borderColor_ = ccc3(255, 0, 0);
    
    redIncreasing_ = FALSE;
    blueIncreasing_ = TRUE;
    greenIncreasing_ = TRUE;
    
    [vertBorderLeft_ setColor:borderColor_];
    [horzBorderBottom_ setColor:borderColor_];
    [vertBorderRight_  setColor:borderColor_];
    [horzBorderTop_ setColor:borderColor_];

	vertBorderLeft_.position = CGPointMake(winSize.width/2.0 - winSize.height/3.0 + 1, winSize.height/2.0);
	horzBorderBottom_.position = CGPointMake(winSize.width/2.0, 0 + controlBoxHeight_);
	[self addChild:vertBorderLeft_];
	[self addChild:horzBorderBottom_];
	vertBorderRight_.position = CGPointMake(winSize.width/2.0 + winSize.height/3.0 - 1, winSize.height/2.0);
	horzBorderTop_.position = CGPointMake(winSize.width/2.0, winSize.height - controlBoxHeight_);
	[self addChild:vertBorderRight_];
	[self addChild:horzBorderTop_];
	
}

-(void)removeSprite:(id)sender {
	CCSprite *sprite = (CCSprite *)sender;
	[self removeChild:sprite cleanup:YES];
}

- (void)onEnter
{
    [super onEnter];
    [[SimpleAudioEngine sharedEngine] resumeAllEffects];
    if (musicPaused_) {
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];

}

- (void)onExit
{
	[[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
    [[SimpleAudioEngine sharedEngine] pauseAllEffects];
    if (singlePlayer_) {
        if (easy_) {
            if (playerScore_ > [GameState sharedInstance].highScoreEasy) {
                [GameState sharedInstance].highScoreEasy = playerScore_;
            }
            [[GameState sharedInstance] save];
            [[GCHelper sharedInstance] reportScore:kLeaderboardOverlordsHS1 score:(unsigned int)playerScore_];
            
        } else {
            if (playerScore_ > [GameState sharedInstance].highScoreHard) {
                [GameState sharedInstance].highScoreHard = playerScore_;
            }
            [[GameState sharedInstance] save];
            [[GCHelper sharedInstance] reportScore:kLeaderboardOverlordsHS101 score:(unsigned int)playerScore_];
        }
    }
	[super onExit];
}	


- (BOOL)containsTouchLocation:(UITouch *)touch pauseBounds:(CGRect)r 
{
    CGPoint p = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
    
    //NSLog(@"Touch %@\n Point: %@\n  Bounds %@\n", touch, NSStringFromCGPoint(p), NSStringFromCGRect(r));
	return CGRectContainsPoint(r, p);
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGRect bounds = CGRectMake(0, winSize.height / 2.33, winSize.width, winSize.height / 7.0);  
    
	if ( ![self containsTouchLocation:touch pauseBounds:bounds] || attractMode_ || gamePaused_ || roundOver_) return NO;
    
    self.isTouchEnabled = NO; //disable tap, (Menu is enabled)
    [self doPause];
 
    return YES;
 }

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
    
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self ccTouchEnded:touch withEvent:event];
}

- (void)dealloc
{
    [balls_ release];
    [shieldTexture_ release];
    [shieldTexture180_ release];
    [shieldTextureGlow_ release];
    [shipTexture_ release];
    [alienRelaxedTexture_ release];
    [alienScrunchTexture_ release];
    [scrollingStars1_ release];
    [scrollingStars2_ release];
    [scrollingStars3_ release];
    [scrollingStars4_ release];
    [scrollingStars5_ release];
    [scrollingStars6_ release];
    [brickExplosion_ release];
    [brickExplosion2_ release];
    [finalExplosion_ release];
    [ringExplosion_ release];
    [plasmaBall_ release];
    [plasmaBall2_ release];
    [electrons_ release];
    [electrons2_ release];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/blip.caf")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/ship-fly-in-160.mp3")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/ship-fly-out-160.mp3")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/ship-cruising-160.mp3")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/brick-explosion2.caf")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/shield-bounce3.caf")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/shield-grab2.caf")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/WilhelmScream.caf")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/final-explosion3.caf")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/intro-alien-voiceover.mp3")];
    [[SimpleAudioEngine sharedEngine] unloadEffect:gameResourcePath(@"sound/nice-move-voiceover.mp3")];
    [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[super dealloc];
}

@end
