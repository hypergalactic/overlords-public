//
//  MathFunctions.m
//  Overlords
//
//  Created by Robert Wasmann on 12-04-13.
//  Copyright 2012 HyperGalactic Games Corporation. All rights reserved.
//

#import "MathFunctions.h"

/* lineIntersectCircle calculates the points of intersection between a line and a circle. 
    Input:  linePoint is a x,y cartesion point on the line. 
            lineSlope is the slope stored as a point (run,rise).
            circleOrigin is the point at the center of the circle
            circleRadius is the radius of the circle
            returnCode is a variable whose address is to be passed in and will be modified by the method. 
            intersection1 is a variable whose address is to be passed in and will be modified by the method.
            intersection2 is a variable whose address is to be passed in and will be modified by the method.
            
    Output: returnCode will be 0 if no real roots or intersections are found. 1 if one intersection is found. 
                And 2 of two intersections are found. 
            intersection1 will be updated to contain the first intersection point.
            intersection2 will be updated to contain the second intersection point. 

*/

void lineIntersectCircle(CGPoint linePoint, CGPoint lineSlope, CGPoint circleOrigin, float circleRadius, 
                         short * returnCode, CGPoint * intersection1, CGPoint * intersection2) {
    
    float A;
    float B;
    float C;
    float slope;
    
    // Special Case: Verticle Line
    if (fabsf(0.0 - lineSlope.x) < 0.01) {
        if (circleRadius * circleRadius - (linePoint.x - circleOrigin.x) * (linePoint.x - circleOrigin.x) > 0.0) {
            intersection1->x = linePoint.x;
            intersection1->y = sqrt(circleRadius * circleRadius - (linePoint.x - circleOrigin.x) * 
                                    (linePoint.x - circleOrigin.x)) + circleOrigin.y;
            intersection2->x = linePoint.x;
            intersection2->y = -sqrt(circleRadius * circleRadius - (linePoint.x - circleOrigin.x) * 
                                     (linePoint.x - circleOrigin.x)) + circleOrigin.y;
            *returnCode = 2;
            return;
        } else if (circleRadius * circleRadius - (linePoint.x - circleOrigin.x) * (linePoint.x - circleOrigin.x) < 0.0) {
            *returnCode = 0;
            return;
        } else {
            intersection1->x = linePoint.x;
            intersection1->y = circleOrigin.y;
            *returnCode = 1;
            return;
        }
    }
    // Special Case: Horizontal Line
    if (fabsf(0.0 - lineSlope.y) < 0.01) {
        if (circleRadius * circleRadius - (linePoint.y - circleOrigin.y) * (linePoint.y - circleOrigin.y) > 0.0) {
            intersection1->x = linePoint.y;
            intersection1->y = sqrt(circleRadius * circleRadius - (linePoint.y - circleOrigin.y) * 
                                    (linePoint.y - circleOrigin.y)) + circleOrigin.x;
            intersection2->x = linePoint.x;
            intersection2->y = -sqrt(circleRadius * circleRadius - (linePoint.y - circleOrigin.y) * 
                                     (linePoint.y - circleOrigin.y)) + circleOrigin.x;
            *returnCode = 2;
            return;
        } else if (circleRadius * circleRadius - (linePoint.y - circleOrigin.y) * (linePoint.y - circleOrigin.y) < 0.0) {
            *returnCode = 0;
            return;
        } else {
            intersection1->x = linePoint.y;
            intersection1->y = circleOrigin.x;
            *returnCode = 1;
            return;
        }
    }
    
    // Cases involving normal lines
    //NSCAssert(lineSlope.x != 0.0, @"lineIntersectCircle: Line Slope X value is Zero.");
    slope = lineSlope.y / lineSlope.x;
    
    A = 1 + slope * slope;
    
    B = 2 * linePoint.y * slope - 2 * circleOrigin.x - 2 * slope * slope * linePoint.x - 2 * circleOrigin.y * slope;
    
    C = circleOrigin.x * circleOrigin.x + slope * slope * linePoint.x * linePoint.x - 2 * linePoint.y * slope * linePoint.x + linePoint.y * linePoint.y 
    + 2 * circleOrigin.y * slope * linePoint.x - 2 * circleOrigin.y * linePoint.y + circleOrigin.y * circleOrigin.y - circleRadius * circleRadius;

    float det = B * B - 4 * A * C;
    
    if (det > 0) { // Intersects at two points. Double root
        *returnCode = 2; 
        intersection1->x = (-B + sqrt(det)) / (2*A);
        intersection2->x = (-B - sqrt(det)) / (2*A);
        intersection1->y = slope * (intersection1->x - linePoint.x) + linePoint.y;
        intersection2->y = slope * (intersection2->x - linePoint.x) + linePoint.y;
        return;
    } else if (det < 0) {  // No intersection, no real roots
        *returnCode = 0;
        return;
    } else if (det == 0) { // Single Intersection, single root, tangent to circle (rare)
        *returnCode = 1; 
        intersection1->x = -B / (2 * A);
        intersection1->y = slope * (intersection1->x - linePoint.x) + linePoint.y;
        return;
    }

}

