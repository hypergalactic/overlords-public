//
//  AccessDefaults.h
//  Overlords
//
//  Created by Robert Wasmann on 12-12-30.
//
//

#import <Foundation/Foundation.h>

#define kDefaultMusicPausedKEY @"kDefaultMusicPausedKEY"

@interface AccessDefaults : NSObject

@property (nonatomic, assign) BOOL musicPaused;

+ (void) defaultSetMusicPaused :(BOOL)paused;
+ (BOOL) defaultGetMusicPaused;

@end
