//
//  AccessDefaults.m
//  Overlords
//
//  Created by Robert Wasmann on 12-12-30.
//
//

#import "AccessDefaults.h"

@implementation AccessDefaults

+ (void) defaultSetMusicPaused:(BOOL)paused;
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:paused forKey:kDefaultMusicPausedKEY];
}
+ (BOOL) defaultGetMusicPaused;
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	BOOL value = (BOOL)[defaults boolForKey:kDefaultMusicPausedKEY];
	[defaults synchronize]; //---IMPORTANT--
	return value;
}


@end
