//
//  GCHelper.h
//  Overlords
//
//  Created by Robert Wasmann on 12-08-22.
//
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import  "cocos2d.h"

#define kAchievementLevel1 @"com.hypergalactic.overlords.achievement.level1"
#define kAchievementLevel5 @"com.hypergalactic.overlords.achievement.level5"
#define kAchievementLevel10 @"com.hypergalactic.overlords.achievement.level10"
#define kAchievementLevel25 @"com.hypergalactic.overlords.achievement.level25"
#define kAchievementLevel50 @"com.hypergalactic.overlords.achievement.level50"
#define kAchievementLevel100 @"com.hypergalactic.overlords.achievement.level100"
#define kAchievementLevel1000 @"com.hypergalactic.overlords.achievement.level1000"
#define kLeaderboardOverlordsHS1 @"com.hypergalactic.overlords.highscores1"  // Easy
#define kLeaderboardOverlordsHS101 @"com.hypergalactic.overlords.highscores1.0.1"  // Hard


@interface GCHelper : NSObject <NSCoding> {
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
    NSMutableArray *scoresToReport;
    NSMutableArray *achievementsToReport;
}

@property (retain) NSMutableArray *scoresToReport;
@property (retain) NSMutableArray *achievementsToReport;

+ (GCHelper *) sharedInstance;
- (void)save;
- (BOOL)isGameCenterAvailable;
- (id)initWithScoresToReport:(NSMutableArray *)scoresToReport achievementsToReport:(NSMutableArray *)achievementsToReport;
- (void)authenticationChanged;
- (void)resendData;
- (void)authenticateLocalUser;
- (void)reportAchievement:(NSString *)identifier percentComplete:(double)percentComplete;
- (void)reportScore:(NSString *)identifier score:(unsigned int)score;

@end