//
//  Shield.h
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import "cocos2d.h"
#import "Ball.h"


typedef enum tagShieldState {
	kShieldStateGrabbed,
	kShieldStateUngrabbed,
    kShieldStateMagnetronOn
} ShieldState;

@interface Shield : CCSprite <CCTargetedTouchDelegate> {
@private
    Ball *currentBall_; // Closest ball
	BOOL inverted_; // is this shield upside down? (enemy)
    BOOL aiPlayer_; // is this an AI controlled shield?
    BOOL interceptCalculated_; // Already calulated AI shield's intercept position
    BOOL newReaction_; // Choose a new reaction?
    BOOL magnetronPowered_; // magnetron has power to operate
    BOOL holdingBall_; // Self holding the ball?
    BOOL AIMagnetronOn_; // AI wants the magnetron on
    BOOL AIswitchback_; // Has the AI switched direction during a maneuvre? 
    BOOL shieldReset_; // True when a reset was just done
    double elapsedTimes_[4];
    short velIndex_;
    short returnCode; // Predicted intersection return code
    short tickCount_;  // Used to decide when AI should release ball
    short count_; // How many ticks should the AI hold the ball
    short reaction_; // What AI reaction is currently being executed
    short magnetronPower_; // Magnetron's Power level in percent. 
    short rand_minus20_20_; // Random Variance modifier to add to AI's x position
    float initTextureRadius_; // Holds the width of the main texture at init
    float magnetronTime_; // Time magnetron has been active 
    float angle_;
    float accel_; // AI's shield acceleration
    float vel_; // AI's shield velocity
    CGPoint intersection_; // Predicted intersection of AI shield and plasma ball 
    CGPoint prevBallVel_; // Ball's velocity in previous tick
    float prevYPosDiff_; // AI's position difference relative to ball in previous tick
    float prevXPosDiff_; // AI's position difference relative to ball in previous tick
    float shieldDefaultYPosition_;
    float shieldLocations_[4];
    float shieldArcRadius_;
    float xVel_;  // shield's average x velocity for previous 4 ticks
    CGPoint shieldOrigin_;
    CGRect textureRect_;
    CGPoint touchPoint1_;
    CGPoint touchOffset1_;
    UITouch *touchObject1_;
    UITouch *touchObject2_;
    ShieldState state_;    
    id magOnSeq_;
    id AIMagnetronOnTrue_;
    id wait_;
}
@property(nonatomic) BOOL inverted;
@property(nonatomic, readwrite, assign) BOOL aiPlayer;
@property(nonatomic, readwrite, assign) BOOL holdingBall;
@property(nonatomic, readwrite, assign) short magnetronPower;
@property(nonatomic, readwrite, assign) float magnetronTime;
@property(nonatomic, readonly) float angle;
@property(nonatomic, readonly) float initTextureRadius;
@property(nonatomic, readonly) float xVel;
@property(nonatomic, readonly) float radius;
@property(nonatomic, readonly) CGRect rect;
@property(nonatomic, readonly) CGRect rectInPixels;
@property(nonatomic, readonly) CGRect textureRect;
@property(nonatomic, readwrite, assign) CGPoint touchPoint1;
@property(nonatomic, readonly) ShieldState state;

+ (id)shieldWithTexture:(CCTexture2D *)texture inverted:(BOOL)inv;
- (id)initWithTexture:(CCTexture2D *)aTexture inverted:(BOOL)inv;
- (void)reset;
- (void)calcNextMove:(NSArray*)balls currLevel:(short)level dt:(ccTime)delta score:(unsigned int)score 
       enemyPosition:(CGPoint)enemyPosition enemyHoldingBall:(BOOL)enemyHoldingBall easy:(BOOL)easyMode;
- (void)move;
- (void)update:(ccTime)delta;
- (void)wasPaused;

@end
