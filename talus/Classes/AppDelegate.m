//
//  AppDelegate.m
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import "cocos2d.h"

#import "AppDelegate.h"
#import "GameConfig.h"
#import "OverlordsScene.h"
#import "RootViewController.h"
#import "GCHelper.h"
#import "Appirater.h"
#import "AccessDefaults.h"

@implementation AppDelegate

@synthesize window = window_;
@synthesize viewController = viewController_;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController

//	CC_ENABLE_DEFAULT_GL_STATES();
//	CCDirector *director = [CCDirector sharedDirector];
//	CGSize size = [director winSize];
//	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
//	sprite.position = ccp(size.width/2, size.height/2);
//	sprite.rotation = -90;
//	[sprite visit];
//	[[director openGLView] swapBuffers];
//	CC_ENABLE_DEFAULT_GL_STATES();
	
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController	
}
- (void) applicationDidFinishLaunching:(UIApplication*)application
{
	// Init the window
	window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //Old code I used to try and modify the touch offset.
/*    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {

        CGRect frame = window.frame;
        CGRect bounds = window.bounds;
        frame.origin.y =  -10;
        frame.size.height = 500;
        bounds.origin.y = -10;
        bounds.size.height = 500;
        window.frame = frame;
        window.bounds = bounds;
    }
*/
    
    // Initialize Game Center
    [[GCHelper sharedInstance] authenticateLocalUser];
    
    // Initialize Appirater
    [Appirater appLaunched:NO];
    
    
    // Try to use CADisplayLink director
	// if it fails (SDK < 3.1) use the default director
	if( ! [CCDirector setDirectorType:kCCDirectorTypeDisplayLink] )
		[CCDirector setDirectorType:kCCDirectorTypeDefault];
	
	
	CCDirector *director = [CCDirector sharedDirector];
	
	// Init the View Controller
	viewController_ = [[RootViewController alloc] initWithNibName:nil bundle:nil];
	viewController_.wantsFullScreenLayout = YES;
	
	//
	// Create the EAGLView manually
	//  1. Create a RGB565 format. Alternative: RGBA8
	//	2. depth format of 0 bit. Use 16 or 24 bit for 3d effects, like CCPageTurnTransition
	//
	//
	EAGLView *glView = [EAGLView viewWithFrame:[window_ bounds]
								   pixelFormat:kEAGLColorFormatRGB565	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];

/*
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        CGRect glBounds = glView.bounds;
        glBounds.origin.y = 10;
        glBounds.size.height = 480;
        glView.bounds = glBounds;
    }
 */
    
	// attach the openglView to the director
	[director setOpenGLView:glView];
    
    // Set multiple touches on
	[glView setMultipleTouchEnabled:YES];
    
    // Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
    
	//if( ! [director enableRetinaDisplay:YES] )
	//	CCLOG(@"Retina Display Not supported");
	
    if(UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad)
    {
        if ([[CCDirector sharedDirector] enableRetinaDisplay:YES])
        {
            NSLog(@"Retina Display Supported");
        }
    }

	
	//
	// VERY IMPORTANT:
	// If the rotation is going to be controlled by a UIViewController
	// then the device orientation should be "Portrait".
	//
	// IMPORTANT:
	// By default, this template only supports Landscape orientations.
	// Edit the RootViewController.m file to edit the supported orientations.
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
#else
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
#endif
	
	[director setAnimationInterval:1.0/60];
	[director setDisplayFPS:NO];
	
	
	// make the OpenGLView a child of the view controller
	[viewController_ setView:glView];
    
	
	// make the View Controller a child of the main window
	[window_ addSubview: viewController_.view];
	
	[window_ makeKeyAndVisible];
    
    // Logs the frame and bounds size
    //NSLog(@"Bounds: %@ Frame: %@", NSStringFromCGRect(glView.bounds), NSStringFromCGRect(window_.frame));
	//CGRect frameTest, boundsTest;
	//frameTest = glView.frame;
	//boundsTest = glView.bounds;
	//NSLog(@"Frame origin: %f, %f Bounds origin: %f, %f", frameTest.origin.x, frameTest.origin.y, boundsTest.origin.x, boundsTest.origin.y);
	//NSLog(@"window: %@", [window_ description]);
    //NSLog(@"window subviews: %@", [[window_ subviews] description]);
	
	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	
	// Removes the startup flicker
	[self removeStartupFlicker];
	
	// Run the intro Scene
	[[CCDirector sharedDirector] runWithScene: [OverlordsScene node]];
}


- (void)applicationWillResignActive:(UIApplication *)application {
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
    //[[CCTextureCache sharedTextureCache] removeAllTextures];

}

-(void) applicationDidEnterBackground:(UIApplication*)application {
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	[[CCDirector sharedDirector] startAnimation];
    [Appirater appEnteredForeground:NO];
    
    // Must call isBackgroundMusicPlaying and setMode or music won't resume after ipod use
    if (![[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]) {
        //[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(resumeMusic) userInfo:nil repeats:NO];
        [[CDAudioManager sharedManager] setMode:kAMM_FxPlusMusicIfNoOtherAudio];
        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    }
    
    // Check to see if music should be paused
    BOOL isPaused = [AccessDefaults defaultGetMusicPaused];
    if (isPaused) {
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];

    }

}

- (void)applicationWillTerminate:(UIApplication *)application {
	CCDirector *director = [CCDirector sharedDirector];
	
	[[director openGLView] removeFromSuperview];
	
	[viewController_ release];
	
	[window_ release];
	
	[director end];	
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
	[[CCDirector sharedDirector] end];
	[window_ release];
	[super dealloc];
}

@end
