//
//  OverlordsScene.h
//  Overlords
//
//  Created by Robert Wasmann on 10-06-28.
//  Copyright HyperGalactic Games Corporation 2010. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "SimpleAudioEngine.h"
#import <GameKit/GameKit.h>


typedef enum {
    kEndReasonWin,
    kEndReasonLose
} EndReason;

@interface OverlordsScene : CCScene {
@private
}
@end

@class Ball;
@class Shield;
@class SneakyJoystick;
@class SneakyButton;
@class ParticleStars;
@class ParticlePlasmaBall;
@class ParticleBrickExplosion;
@class ParticleFinalExplosion;
@class ParticleRingExplosion;
@class ParticleElectrons;


@interface OverlordsLayer: CCLayer <GKLeaderboardViewControllerDelegate, GKAchievementViewControllerDelegate> {
@private

    ALuint shipsLastSoundID_;
    ALuint lastSoundID_;
    short lives_;  
    unsigned short level_;
    unsigned short AILevel_; // Level the AI plays at
    unsigned int playerScore_;
    unsigned int enemyScore_;
    short colorDeltaR_; // For border color cycling
    short colorDeltaB_;
    short colorDeltaG_;
    short countDown_;
    short nextTrack_; // Which background music track to play next?
    short brickExplosionNum_; // which brick particle effect to use next.
    float controlBoxHeight_;
    float sysVersion_; // Holds the system OS version number
    float accumulatedTime_;
    float colorTime_; // Accumulated time for border color changing.
    float alienAnimationTime_;
    float slowBallSpeed_;
    float fastBallSpeed_;
    float hyperBallSpeed_;
    float initialAngle_; // intial angle the plasmoid is released at.
    BOOL alienSpinning_; // Is the alien currently spinning?
    BOOL alienSpinningTextureDisplayed_; // Is the alien currently displaying the spinning texture?
    BOOL alienTextureFlipped_; // Have we flipped the texture?
    BOOL redIncreasing_; // For border color cycling
    BOOL greenIncreasing_;
    BOOL blueIncreasing_;
    BOOL easy_; // Easy game play mode?
    bool gameActive_; // Is the game in play?
    bool roundOver_; // Is the round over and end menu showing?
    bool gameOver_;
    BOOL greemoScheduled_; // Is Greemo(ship) scheduled to come fly in?
    BOOL musicPaused_; // Is the background music currently paused?
    bool newGame_;
    bool newHighScore_;
    bool singlePlayer_;
    bool twoPlayer_;
    bool attractMode_;
    bool playerGlow_;
    bool enemyGlow_;
    bool gamePaused_;
    bool enemyBlown_; // Is enemy side blown up?
    bool playerBlown_; // Is player side blown up?
    bool twoPlayerFirstRound_; // Is this the first round in two player mode?
    CGPoint ballStartingVelocity_;
    NSArray *balls_;
    NSNumber *NSInitialAngle_;
    NSNumber *NSInitialAngle2_;
	Shield *playerShield_;
	Shield *enemyShield_;
    CCSprite *ship_;
    CCSprite *vertBorderLeft_; // Game Border
    CCSprite *horzBorderBottom_; // Game Border
    CCSprite *vertBorderRight_; // Game Border
    CCSprite *horzBorderTop_; // Game Border
    CCSprite *loadingScreen_;
    CCAction *alienScrunchAction_;
    ccColor3B borderColor_;
    
    CDSoundSource *song1_;
    CDSoundSource *song2_;
    CDSoundSource *song3_;
    CDSoundSource *song4_;
    CDSoundSource *song5_;

    Ball *ball_;
    Ball *ball2_;
    Ball *ball3_;
    Ball *ball4_;
    Ball *ball5_;
    Ball *ball6_;
    Ball *closestBallPlayer_;
    Ball *closestBallEnemy_;
    ParticleStars *scrollingStars1_;
    ParticleStars *scrollingStars2_;
    ParticleStars *scrollingStars3_;
    ParticleStars *scrollingStars4_;
    ParticleStars *scrollingStars5_;
    ParticleStars *scrollingStars6_;
    ParticlePlasmaBall *plasmaBall_;
    ParticlePlasmaBall *plasmaBall2_;
    ParticlePlasmaBall *plasmaBall3_;
    ParticlePlasmaBall *plasmaBall4_;
    ParticlePlasmaBall *plasmaBall5_;
    ParticlePlasmaBall *plasmaBall6_;
    ParticleBrickExplosion *brickExplosion_;
    ParticleBrickExplosion *brickExplosion2_;
    ParticleFinalExplosion *finalExplosion_;
    ParticleRingExplosion *ringExplosion_;
    ParticleElectrons *electrons_;
    ParticleElectrons *electrons2_;
    CCTMXTiledMap *playerMap_;
    CCTMXTiledMap *enemyMap_;
    CCTMXLayer *playerLayer_;
    CCTMXLayer *enemyLayer_;
    CCTexture2D *shieldTextureGlow_;
    CCTexture2D *shieldTexture_;
    CCTexture2D *shieldTexture180_;
    CCTexture2D *shipTexture_;
    CCTexture2D *alienScrunchTexture_;
    CCTexture2D *alienRelaxedTexture_;
    CCTexture2D *alienSpinningTexture_;

    CCLabelBMFont *label_;
    CCLabelBMFont *label2_;
    CCLabelBMFont *label3_;
    CCLabelBMFont *playerScoreLabel_;
    CCLabelBMFont *playerMagLvlLabel_;
    CCLabelBMFont *enemyMagLvlLabel_;
    CCLabelBMFont *playerLivesLabel_;
    CCLabelBMFont *levelLabel_;
    CCLabelBMFont *enemyScoreLabel_;
    CCMenu *menu_;
    EndReason endReason_;
}


-(void)restartTapped:(id)sender;
-(void)showLeaderboard:(id)sender;
-(void)showAchievements:(id)sender;
-(void)soundOffTapped:(id)sender;
-(void)musicOffTapped:(id)sender;
-(void)doPause;
-(void)pauseLayerDidPause;
-(void)pauseLayerDidUnpause;

@end
