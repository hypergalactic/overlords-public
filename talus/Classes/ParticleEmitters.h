//
//  ParticleEmitters.h
//  Overlords
//
//  Created by Robert Wasmann on 11-11-15.
//  Copyright 2011 HyperGalactic Games Corporation. All rights reserved.
//
#import <Availability.h>
#import "cocos2d.h"
#import "ParticleSystemPoint.h"
#import "ParticleSystemQuad.h"


// build each architecture with the optimal particle system

// ARMv7, Mac or Simulator use "Quad" particle
#if defined(__ARM_NEON__) || defined(__MAC_OS_X_VERSION_MAX_ALLOWED) || TARGET_IPHONE_SIMULATOR
#define ARCH_OPTIMAL_PARTICLE_SYSTEM2 ParticleSystemQuad

// ARMv6 use "Point" particle
#elif __arm__
#define ARCH_OPTIMAL_PARTICLE_SYSTEM2 ParticleSystemPoint
#else
#error(unknown architecture)
#endif

//! A star particle system
@interface ParticleStars: ARCH_OPTIMAL_PARTICLE_SYSTEM2 {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end

@interface ParticleGalaxies: ARCH_OPTIMAL_PARTICLE_SYSTEM2 {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end

@interface ParticleBrickExplosion: ARCH_OPTIMAL_PARTICLE_SYSTEM2 {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end

@interface ParticleFinalExplosion: ARCH_OPTIMAL_PARTICLE_SYSTEM2 {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end

@interface ParticleRingExplosion: ARCH_OPTIMAL_PARTICLE_SYSTEM2 {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end


// USING STANDARD COCOS2D PARTICLE SYSTEM

// build each architecture with the optimal particle system

// ARMv7, Mac or Simulator use "Quad" particle
#if defined(__ARM_NEON__) || defined(__MAC_OS_X_VERSION_MAX_ALLOWED) || TARGET_IPHONE_SIMULATOR
#define CC_ARCH_OPTIMAL_PARTICLE_SYSTEM CCParticleSystemQuad

// ARMv6 use "Point" particle
#elif __arm__
#define CC_ARCH_OPTIMAL_PARTICLE_SYSTEM CCParticleSystemPoint
#else
#error(unknown architecture)
#endif

@interface ParticlePlasmaBall: CC_ARCH_OPTIMAL_PARTICLE_SYSTEM {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end

@interface ParticleElectrons: CC_ARCH_OPTIMAL_PARTICLE_SYSTEM {
}
-(id) initWithTotalParticles:(NSUInteger)p;
-(id) initWithTotalParticles:(NSUInteger)p withTexture:(NSString *)textureName;
@end


