//
//  MathFunctions.h
//  Overlords
//
//  Created by Robert Wasmann on 12-04-13.
//  Copyright 2012 HyperGalactic Games Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

/* lineIntersectCircle calculates the points of intersection between a line and a circle. 
 Input:  linePoint is a x,y cartesion point on the line. 
 lineSlope is the slope stored as a point (run,rise).
 circleOrigin is the point at the center of the circle
 circleRadius is the radius of the circle
 returnCode is a variable whose address is to be passed in and will be modified by the method. 
 intersection1 is a variable whose address is to be passed in and will be modified by the method.
 intersection2 is a variable whose address is to be passed in and will be modified by the method.
 
 Output: returnCode will be 0 if no real roots or intersections are found. 1 if one intersection is found. 
 And 2 of two intersections are found. 
 intersection1 will be updated to contain the first intersection point.
 intersection2 will be updated to contain the second intersection point. 
 
*/
void lineIntersectCircle(CGPoint linePoint, CGPoint lineSlope, CGPoint circleOrigin, float circleRadius, 
            short * returnCode, CGPoint * intersection1, CGPoint * intersection2);


