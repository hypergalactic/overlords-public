//
//  Shield.m
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import "Shield.h"
#import "cocos2d.h"
#include "MathFunctions.h"

#define controlBoxRatio 0.04166f //0.0833f // Height ratio of the controller box
#define shieldYPosRatio 2.7f    // Max Shield Y-axis screen position as a 1/x ratio of the screen height
#define shieldRadiusRatio 1.25f // Determines Shield arc radius as a 1/x ratio of the screen height
#define SJ_PI_X_2 6.28318530718f
#define PI_DIV_2 1.5707963268f
#define xVelMod 0.5f // User's touch x velocity will be multiplied by this amount. 
#define initVel 150.0f // AI's initial velocity
#define maxVel 300.0f // AI's max velocity at level peakLevel
#define initFlickVel 250.0f // AI's initial max flick velocity
#define flickVel 400.0f // AI's max velocity for a flick move
#define peakLevel 100.0f // Peak level for difficulty curve
#define centreVariance 0.45f // How close shield can get to centre screen before use of special cases to avoid infinite slopes. 




@implementation Shield
@synthesize inverted = inverted_, touchPoint1 = touchPoint1_, state = state_, angle = angle_, xVel = xVel_, holdingBall = holdingBall_,
            textureRect = textureRect_, initTextureRadius = initTextureRadius_, aiPlayer = aiPlayer_, magnetronTime = magnetronTime_, magnetronPower = magnetronPower_;

- (float)radius
{
    CGSize s = [texture_ contentSize];
	//return textureRect_.size.width / 2.0;
    return s.width / 2.0;
}

- (CGRect)rectInPixels
{
	CGSize s = [texture_ contentSizeInPixels];
	return CGRectMake(0, 0, s.width, s.height);
}

- (CGRect)rect
{
	CGSize s = [texture_ contentSize];
	return CGRectMake(0, 0, s.width, s.height);
}

+ (id)shieldWithTexture:(CCTexture2D *)aTexture inverted:(BOOL)inv
{
	return [[[self alloc] initWithTexture:aTexture inverted:inv] autorelease];
}

- (id)initWithTexture:(CCTexture2D *)aTexture inverted:(BOOL)inv
{
	if ((self = [super initWithTexture:aTexture]) ) {
        inverted_ = inv;
        initTextureRadius_ = [self radius];
        [self reset];
    }
	return self;
}

- (void)reset {
    shieldReset_ = TRUE;
    interceptCalculated_ = FALSE;
    newReaction_ = TRUE;
    AIMagnetronOn_ = TRUE;
    AIswitchback_ = FALSE;
    magnetronPowered_ = TRUE;
    holdingBall_ = FALSE;
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    textureRect_ = [self rect];
    state_ = kShieldStateUngrabbed;
    shieldArcRadius_ = winSize.height / shieldRadiusRatio;
    magnetronTime_ = 0.0;
    magnetronPower_ = 100;

    self.visible = TRUE;
    
    wait_ = [[CCDelayTime actionWithDuration:0.05] retain];
    AIMagnetronOnTrue_ = [[CCCallFuncN actionWithTarget:self selector:@selector(setAIMagnetronOnTrue:)] retain];
    magOnSeq_ = [[CCSequence actions:wait_, AIMagnetronOnTrue_, nil] retain];

    
    if (!inverted_) {
        shieldDefaultYPosition_ = winSize.height / shieldYPosRatio;
        shieldOrigin_ = ccp(winSize.width / 2.0, shieldDefaultYPosition_ - shieldArcRadius_);
        
    } else if (inverted_) {
        shieldDefaultYPosition_ = winSize.height - winSize.height / shieldYPosRatio;
        shieldOrigin_ = ccp(winSize.width / 2.0, shieldDefaultYPosition_ + shieldArcRadius_);
        
    }

    for (int i = 0; i < 4; i++) {
        shieldLocations_[i] = 0.0f;
        elapsedTimes_[i] = 0.0f;
    }
    velIndex_ = 0;
    //touchBeginPoints = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, &kCFTypeDictionaryKeyCallBacks, NULL);
    touchObject1_ = NULL;
    touchObject2_ = NULL;
    angle_ = 0.0f;

}

- (void)calcNextMove:(NSArray*)balls currLevel:(short)level dt:(ccTime)delta score:(unsigned int)score 
enemyPosition:(CGPoint)enemyPosition enemyHoldingBall:(BOOL)enemyHoldingBall easy:(BOOL)easyMode {

    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    // Make sure the shield is always Grabbed at the minimum.
    if (state_ == kShieldStateUngrabbed) {
        state_ = kShieldStateGrabbed;
    }
    
    // Find closest ball to track, reset AI when a better choice is found
    Ball *ball = currentBall_;
    Ball *potentialBall = [self ballToTrack:balls];
    
    if (potentialBall != currentBall_) {
    
        float currDistSQ = ccpLengthSQ(ccpSub(currentBall_.position, self.position));
        float currYDist = fabsf(currentBall_.position.y - self.position.y);
        float newDistSQ = ccpLengthSQ(ccpSub(potentialBall.position, self.position));
        float minYDiff = winSize.height / 8.0; // Must be further away by this amount or keep currentBall
        float minDiffSQ;
        
        // Select a minDiffSQ value based on distance to current ball. (To avoid Sq Root function)
        if (currDistSQ > (winSize.height / 2.4) * (winSize.height / 2.4)) {
            minDiffSQ = 7600.0f; // equiv to 20 points at 200 points current distance
        } else if (currDistSQ > (winSize.height / 4.8) * (winSize.height / 4.8) && currDistSQ <= 
                   (winSize.height / 2.4) * (winSize.height / 2.4)) {
            minDiffSQ = 3600.0f; // equiv to 20 points at 100 points current distance
        } else if (currDistSQ > (winSize.height / 9.6) * (winSize.height / 9.6) && currDistSQ <= 
                   (winSize.height / 4.8) * (winSize.height / 4.8)) {
            minDiffSQ = 1200.0f; // equiv to 20 points at 50 points current distance
        } else {
            minDiffSQ = 500.0f; // equiv to 20 points at 25 points current distance
        }
        
        if (IS_IPAD) {
            minDiffSQ *= 2.133f;
        }
        
        // If holding ball and another Visible ball is to intersect close by, then let the ball go so we can block the incoming one
        // Only if it's less than 100 points away, otherwise continue about our business.
        if ((newDistSQ < (winSize.height / 4.8) * (winSize.height / 4.8)) && potentialBall.visible) {
            float Xintersect = (self.position.y - potentialBall.position.y + (potentialBall.velocity.y / potentialBall.velocity.x) 
                                * potentialBall.position.x) * (potentialBall.velocity.x / potentialBall.velocity.y);
            
            if (fabsf(self.position.x - Xintersect) < winSize.height / 8.0 && holdingBall_ && potentialBall.shieldHolding != self) {
                [self reactionDone];
            }
        }

        if (!holdingBall_ && potentialBall.visible) {
            float newYDist = fabsf(potentialBall.position.y - self.position.y);
            
            // Only track new ball if it's significantly closer and moving towards shield
            // or if current ball is now behind shield or current ball is moving away to other end while potential is approaching.
            if (((newDistSQ < (currDistSQ - minDiffSQ)) && ((potentialBall.velocity.y < 0 && !inverted_) 
                || (potentialBall.velocity.y > 0 && inverted_))) 
                
                || (currentBall_.position.y > self.position.y && inverted_ && potentialBall.position.y < self.position.y) 
                || (currentBall_.position.y < self.position.y && !inverted_ && potentialBall.position.y > self.position.y)
                || (currentBall_.velocity.y < 0 && inverted_ && potentialBall.velocity.y > 0) 
                || (currentBall_.velocity.y > 0 && !inverted_ && potentialBall.velocity.y < 0)) {
                
                ball = potentialBall;
                currentBall_ = potentialBall;
                interceptCalculated_ = FALSE;
                
            // Else if both balls moving away, track furthest one if it's significantly further away   
            } else if ((newYDist > currYDist + minYDiff) && ((currentBall_.velocity.y > 0 && 
                potentialBall.velocity.y > 0 && !inverted_) || (currentBall_.velocity.y < 0 && 
                potentialBall.velocity.y < 0 && inverted_))) {
                
                ball = potentialBall;
                currentBall_ = potentialBall;
                interceptCalculated_ = FALSE;
                
            // Else if current ball is now invisible and potential ball is the visible one
            } else if (potentialBall.visible && !currentBall_.visible) {
                ball = potentialBall;
                currentBall_ = potentialBall;
                interceptCalculated_ = FALSE;
            }
        }
    }
    
    if (!ball.visible) {
        touchPoint1_ = ccp(winSize.width / 2.0, winSize.height / 2.0);
        return;
    }
    
    CGPoint ballPos = ball.position;
    CGPoint ballVel = ball.velocity;
    
    float yPositionDiff = ballPos.y - self.position.y;
    float xPositionDiff = ballPos.x - self.position.x;
    // maximum universal velocity adjusted below to device size.
    float maxUniVel = ((flickVel - initFlickVel)/log(peakLevel)) * log(level) + initFlickVel;
    
    if (easyMode) {
        maxUniVel = maxUniVel * 0.75;
    }

    if (IS_IPAD) {
        maxUniVel = maxUniVel * 2.133; // Maximum Universal Velocity
    }
    
    // Starts at initVel at lvl 1 and ramps up to maxVel at level peakLevel using a natural log curve
    float currVel = ((maxVel - initVel)/log(peakLevel)) * log(level) + initVel;
    
    if (easyMode) {
        currVel = currVel * 0.75;
    }

    if (IS_IPAD) {
        currVel = currVel * 2.133;
    }
    
    if (currVel > maxUniVel) currVel = maxUniVel;
    
    // Decide what to do when the ball is caught. Add one new reaction for each level gained.
    if (holdingBall_) {
        
        if (newReaction_) {
            
            //reset acceleration arrays 
            [self resetXVelArray];

            // Set how many ticks the AI should hold onto the ball for once caught
            count_ = (arc4random() % 21) + 5;
            tickCount_ = 0;
            int reactionLevel;
            
            if (level > 5) {
                reactionLevel = 5;
            } else {
                reactionLevel = level;
            }
            reaction_ = (arc4random() % reactionLevel) + 1;  // Random number between 1 and current level.

            if (reaction_ > 5) reaction_ = 5;
            
            if (reaction_ == 3) {
                count_ = 20;
            } else if (reaction_ == 4) {
                count_ = 30;
            }
            newReaction_ = FALSE;
        }
        
        switch (reaction_) {
            case 1:  // Move in random direction, release if edge of arena is encountered 
                if (tickCount_ >= count_ || (tickCount_ > 2 && (self.position.x <= winSize.width/2.0 - winSize.height/3.0 + winSize.height/48.0
                                 || self.position.x >= winSize.width/2.0 + winSize.height/3.0 - winSize.height/48.0))) {
                    [self reactionDone];
                    return;
                }
                
                // If rand_minus20_20_ negative, move negative x direction
                if (fabsf(rand_minus20_20_) != rand_minus20_20_) {  
                    vel_ = -currVel;
                    if (fabsf(vel_) > maxUniVel) {
                        vel_ = -maxUniVel;
                    }
                } else {
                    // Shield moves in positive x direction
                    vel_ = currVel;
                    if (vel_ > maxUniVel) {
                        vel_ = maxUniVel;
                    }
                }
                tickCount_ ++;
                [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
                return;
                break;
                
            case 2:  // Fast flick in random direction, release if edge of arena is encountered
                if (tickCount_ >= count_ / 2.0 || (tickCount_ > 2 && (self.position.x <= winSize.width/2.0 - winSize.height/3.0 + winSize.height/48.0
                                  || self.position.x >= winSize.width/2.0 + winSize.height/3.0 - winSize.height/48.0))) {
                    [self reactionDone];
                    return;
                }
                
                // If rand_minus20_20_ negative, move negative x direction
                if (fabsf(rand_minus20_20_) != rand_minus20_20_) { 
                    vel_ = -flickVel;
                    if (IS_IPAD) {
                        vel_ = vel_ * 2.133;
                    }
                    if (fabsf(vel_) > maxUniVel) {
                        vel_ = -maxUniVel;
                    }
                } else {
                    // Shield moves in positive x direction
                    vel_ = flickVel;
                    if (IS_IPAD) {
                        vel_ = vel_ * 2.133;
                    }

                    if (vel_ > maxUniVel) {
                        vel_ = maxUniVel;
                    }
                }
                tickCount_ ++;
                [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
                return;
                break;
                
            case 3:  // Fast flick in random direction, switching back once, release if edge of arena is encountered
                     // Short slow sweep followed by long fast sweep.
                if (tickCount_ >= count_ || (AIswitchback_ && ((self.position.x <= winSize.width/2.0 - winSize.height/3.0 + winSize.height/48.0)
                                        || (self.position.x >= winSize.width/2.0 + winSize.height/3.0 - winSize.height/48.0)))) {
                    [self reactionDone];
                    return;
                }
                
                if (!AIswitchback_ && (tickCount_ > count_ / 2.0 || (tickCount_ > 2 && ((self.position.x <= winSize.width/2.0 - winSize.height/3.0 + winSize.height/48.0)
                    || (self.position.x >= winSize.width/2.0 + winSize.height/3.0 - winSize.height/48.0))))) {
                    
                    rand_minus20_20_ = -rand_minus20_20_;
                    AIswitchback_ = TRUE;
                }
                
                // If rand_minus20_20_ negative, move negative x direction
                if (fabsf(rand_minus20_20_) != rand_minus20_20_) { 
                    if (!AIswitchback_) {
                        vel_ = -currVel;
                    } else {
                        vel_ = -flickVel;
                    }
                    if (IS_IPAD) {
                        vel_ = vel_ * 2.133;
                    }
                    if (fabsf(vel_) > maxUniVel) {
                        vel_ = -maxUniVel;
                    }
                } else {
                    // Shield moves in positive x direction
                    if (!AIswitchback_) {
                        vel_ = currVel;
                    } else {
                        vel_ = flickVel;
                    }                    
                    if (IS_IPAD) {
                        vel_ = vel_ * 2.133;
                    }
                    
                    if (vel_ > maxUniVel) {
                        vel_ = maxUniVel;
                    }
                }
                tickCount_ ++;
                [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
                return;
                break;
                
            case 4:  // Fast flick in random direction, switching back once, release if edge of arena is encountered
                     // Long slow sweep followed by quick short flick in opposite direction
                if (tickCount_ >= count_ || (AIswitchback_ && ((self.position.x <= winSize.width/2.0 - winSize.height/3.0 + winSize.height/48.0)
                                                               || (self.position.x >= winSize.width/2.0 + winSize.height/3.0 - winSize.height/48.0)))) {
                    [self reactionDone];
                    return;
                }
                
                if (!AIswitchback_ && (tickCount_ > count_ * 0.8 || (tickCount_ > 2 && ((self.position.x <= winSize.width/2.0 - winSize.height/3.0 + winSize.height/48.0)
                                       || (self.position.x >= winSize.width/2.0 + winSize.height/3.0 - winSize.height/48.0))))) {
                    
                    rand_minus20_20_ = -rand_minus20_20_;
                    [self resetXVelArray];
                    AIswitchback_ = TRUE;
                }
                
                // If rand_minus20_20_ negative, move negative x direction
                if (fabsf(rand_minus20_20_) != rand_minus20_20_) { 
                    if (!AIswitchback_) {
                        vel_ = -currVel;
                    } else {
                        vel_ = -flickVel;
                    }
                    if (IS_IPAD) {
                        vel_ = vel_ * 2.133;
                    }
                    if (fabsf(vel_) > maxUniVel) {
                        vel_ = -maxUniVel;
                    }
                } else {
                    // Shield moves in positive x direction
                    if (!AIswitchback_) {
                        vel_ = currVel;
                    } else {
                        vel_ = flickVel;
                    }                    
                    if (IS_IPAD) {
                        vel_ = vel_ * 2.133;
                    }
                    
                    if (vel_ > maxUniVel) {
                        vel_ = maxUniVel;
                    }
                }
                tickCount_ ++;
                [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
                return;
                break;
                
            case 5: // Aim the shot right down the center
                intersection_.x = winSize.width / 2.0;
                intersection_.y = shieldDefaultYPosition_;
                interceptCalculated_ = TRUE;
                if (ccpFuzzyEqual(self.position, intersection_, centreVariance)) {
                    [self reactionDone];
                    xVel_ = 0.0;  // make sure shield's x velocity is set to zero
                    return;
                }
                break;
                    
            default:
                break;
        }
    
    }
    
    // If a wall bounce occurs then recalculate intercept
    if (prevBallVel_.x != ballVel.x || prevBallVel_.y != ballVel.y) {
        interceptCalculated_ = FALSE;
    }
    
    // If ball is behind shield by 20 points, then slowly reduce velocity to 0;
    if (((ballPos.y > self.position.y + winSize.height / 24) && inverted_) 
        || ((ballPos.y < self.position.y - winSize.height / 24) && !inverted_)) {
        
        interceptCalculated_ = FALSE;
        vel_ = vel_ / 1.05;
        [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
        return;
    }
    
    // If ball is moving away from shield OR held, then track it at reduced speed. Exact rebound is likely.
    // Can not use yPosition Difference to check for direction because shield moves on an arc. 
    if ((((ballVel.y > 0 && !inverted_) || (ballVel.y < 0 && inverted_)) && !holdingBall_)
        || ((ball.heldByEnemyShield && !inverted_) || (ball.heldByPlayerShield && inverted_))) {
        
        interceptCalculated_ = FALSE;
        vel_ = currVel / 2.0;

        // If the position difference is less than ~31 points then slow the velocity using a sine function. 
        if (fabsf(xPositionDiff) < PI_DIV_2 * winSize.height / 24.0) {
            vel_ = sinf(xPositionDiff / (winSize.height / 24.0)) * vel_;
        // If position difference is not positive negate the velocity value 
        // so shield moves in negative x direction
        } else if (fabsf(xPositionDiff) != xPositionDiff) {
            vel_ = -vel_;
        }
        [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
        return;
    }
    
    
    // Ball is thus heading towards shield, so predict where the ball is headed to intersect on the 
    // shield's radius of movement. Only happens once and a flag is then set.
    
    if (!interceptCalculated_) {
        CGPoint intersection2;
        returnCode = 4;
        lineIntersectCircle(ballPos, ballVel, shieldOrigin_, shieldArcRadius_, &returnCode, &intersection_, &intersection2);
        if (returnCode == 0) { // No intersection
            if (ballVel.x > 0) {
                intersection_.x = winSize.width / 2.0 + winSize.height / 3.0;
                intersection_.y = shieldDefaultYPosition_;
            } else  if (ballVel.x < 0) {
                intersection_.x = winSize.width / 2.0 - winSize.height / 3.0;
                intersection_.y = shieldDefaultYPosition_;
            }
        } else if (returnCode == 2) { // Two intersection points
            CGPoint v1 = ccpSub(intersection_, ballPos);
            float d1 = ccpLengthSQ(v1);
            CGPoint v2 = ccpSub(intersection2, ballPos);
            float d2 = ccpLengthSQ(v2);
            
            if (d2 < d1) {
                intersection_ = intersection2;
            }
        } else if (returnCode == 4) {
            NSLog(@"ERROR: Shield AI could not determine intersection point");
            intersection_ = CGPointMake(winSize.width / 2.0, shieldDefaultYPosition_);
        }
        
        // Add random variance to intersection depending on level
        // Only if Ball has no velocity to prevent movement at ball launch,
        // and for added realism when launching from static position near center, so it doesn't jump 
        // to an obviously wrong position (from a good position) when the ball is released by the opponent. 
        
        float deadZone = 20.0;
        if (IS_IPAD) {
            deadZone = 5.0 * 2.133;
        }
        
        if ((ballVel.x != 0.0 && ballVel.y != 0.0) 
            && !((self.position.x > enemyPosition.x - deadZone) 
            && (self.position.x < enemyPosition.x + deadZone)
            &&  (enemyPosition.x > winSize.width / 2.0 - deadZone)
            &&  (enemyPosition.x < winSize.width / 2.0 + deadZone))) {
                
            rand_minus20_20_ = (arc4random() % 40) - 20; // Random between -20 and +20
            int xVariance = rand_minus20_20_;
            if (abs(xVariance) != xVariance) {
                xVariance = xVariance + 4.0 * log(level); // Less variance as level increases (natural log curve)
                if (xVariance > 0) xVariance = 0;
            } else {
                xVariance = xVariance - 4.0 * log(level);
                if (xVariance < 0) xVariance = 0;
            }
                
            // Adjust final variance for iPad
            if (IS_IPAD) {
                xVariance = xVariance * 2.133;
            }
            intersection_.x = intersection_.x + xVariance;
        }
        interceptCalculated_ = TRUE;
    }
    float xPredictedIntersectionDiff = intersection_.x - self.position.x;
    
    // If the position difference is less than an amount then slow the velocity using a sine function. 
    if (fabsf(xPredictedIntersectionDiff) < PI_DIV_2 * winSize.height / 24.0) {
        vel_ = sinf(xPredictedIntersectionDiff / (winSize.height / 24.0)) * currVel;
    } else {
        // If position difference is not positive negate the velocity value 
        // so shield moves in negative x direction
        if (fabsf(xPredictedIntersectionDiff) != xPredictedIntersectionDiff) {
            vel_ = -currVel;
            if (fabsf(vel_) > maxUniVel) {
                vel_ = -maxUniVel;
            }
        } else {
            // Shield moves in positive x direction
            vel_ = currVel;
            if (vel_ > maxUniVel) {
                vel_ = maxUniVel;
            }
        }
    }
    [self AISetFinalVars:delta yPosDiff:yPositionDiff xPosDiff:xPositionDiff ballVel:ballVel];
}

- (void)AISetFinalVars:(ccTime)dt yPosDiff:(float)yPosDiff xPosDiff:(float)xPosDiff ballVel:(CGPoint)ballVel {
    touchPoint1_.x = vel_ * dt + self.position.x;
    touchPoint1_.y = self.position.y;
    prevYPosDiff_ = yPosDiff;
    prevXPosDiff_ = xPosDiff;
    prevBallVel_ = ballVel;
}

- (void)reactionDone {
    state_ = kShieldStateGrabbed;
    [self runAction:magOnSeq_];
    AIMagnetronOn_ = FALSE;
    tickCount_ = 0;
    newReaction_ = TRUE;
    AIswitchback_ = FALSE;
}

- (void)resetXVelArray {
    for (int i = 0; i < 4; i++) {
        shieldLocations_[i] = 0.0f;
        elapsedTimes_[i] = 0.0f;
    }
    velIndex_ = 0;
}

- (Ball *)ballToTrack:(NSArray *)balls {
    
    float towardsBallDist = 10000000.0;
    float awayBallDist = 0.0;

    Ball *potentialBall = currentBall_;
    BOOL oneFound = FALSE;
    BOOL oneMovingAway = FALSE;
    BOOL oneMovingTowards = FALSE;
    BOOL allInvisible = TRUE;
    
    for (Ball *ball in balls) {
        
        if (!ball.visible) {
            continue;
        }
        
        // If ball is visible and moving away on the y axis and not behind shield
        if (((ball.velocity.y > 0 && !inverted_) && (ball.position.y >= self.position.y))
            || ((ball.velocity.y < 0 && inverted_) && (ball.position.y <= self.position.y))) {
            
            // Skip if there is a better candidate moving towards the shield already
            if (oneMovingTowards) {
                continue;
            }
            
            allInvisible = FALSE;
   
            //float dist = ccpLengthSQ(ccpSub(ball.position, self.position));
            float dist = fabsf(ball.position.y - self.position.y);
            if (dist > awayBallDist) {
                awayBallDist = dist;
                potentialBall = ball;
                oneFound = TRUE;
                oneMovingAway = TRUE;
            }
            continue;
            
        // Else ball is moving towards shield and not behind it. (prefered)
        } else if (((ball.velocity.y < 0 && !inverted_) && (ball.position.y >= self.position.y))
                || ((ball.velocity.y > 0 && inverted_) && (ball.position.y <= self.position.y))) {
            
            allInvisible = FALSE;
            float dist = ccpLengthSQ(ccpSub(ball.position, self.position));
            if (dist < towardsBallDist) {
                towardsBallDist = dist;
                potentialBall = ball;
                oneFound = TRUE;
                oneMovingTowards = TRUE;
                continue;
            }
        }
        
        // If a ball is visible at all, then use as a last resort
        if (ball.visible && !(oneMovingTowards || oneMovingAway)) {

            allInvisible = FALSE;
            potentialBall = ball;
            oneFound = TRUE;
        }
        
    }
    
    if (allInvisible && !oneFound) {
        for (Ball *ball in balls) {
            potentialBall = ball;  // pick one
            break;
        }
    }
        
    return potentialBall;
}


- (void)move {
    
    float xpos, ypos;
	CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    // If touchpoint is beyond the play area's border then put it back at the border. For the AI's sake.
    if ((touchPoint1_.x < winSize.width/2.0 - winSize.height/3.0) && aiPlayer_) {
        touchPoint1_.x = winSize.width/2.0 - winSize.height/3.0;
    } else if ((touchPoint1_.x > winSize.width/2.0 + winSize.height/3.0) && aiPlayer_) {
        touchPoint1_.x = winSize.width/2.0 + winSize.height/3.0;
    }
    
    // SPECIAL CASE: If near the center of screen then center the shield and return, avoids divide by 0;
    if ((touchPoint1_.x > (winSize.width / 2.0 - centreVariance)) && (touchPoint1_.x < (winSize.width / 2.0 + centreVariance))) {
        self.position = (CGPointMake(winSize.width / 2.0, shieldDefaultYPosition_));
        if (inverted_) {
            angle_ = -PI_DIV_2;
        } else {
            angle_ = PI_DIV_2;
        }
        return;
    }
    
    float dy = touchPoint1_.y - shieldOrigin_.y;

    float dx = touchPoint1_.x - shieldOrigin_.x;

    // Calculate the slope of the line between the shield origin and the touchpoint
    float slope = dy / dx;
    
    float invertedModifier = 1.0f;
    if (inverted_) invertedModifier = -1.0f;

    if (slope != fabsf(slope)) {
        xpos = invertedModifier * -sqrt((shieldArcRadius_ * shieldArcRadius_) / ( 1.0 + slope * slope)) + winSize.width / 2.0;
    }
    else {
        xpos = invertedModifier * sqrt((shieldArcRadius_ * shieldArcRadius_) / ( 1.0 + slope * slope)) + winSize.width / 2.0;
    }
    
    ypos = slope * xpos + shieldOrigin_.y - slope * winSize.width / 2.0;
    
    self.position = CGPointMake(xpos, ypos);
    angle_ = atan2f(dy, dx); // in radians
    if(angle_ < 0){
        angle_ += SJ_PI_X_2;
    }
    
    //If holding a ball, then keep track of shield's x velocity.
    if (holdingBall_) {
        shieldLocations_[velIndex_] = self.position.x;
        elapsedTimes_[velIndex_] = [NSDate timeIntervalSinceReferenceDate];
        if (velIndex_ == 3) {
            xVel_ = ((shieldLocations_[3] - shieldLocations_[2]) + (shieldLocations_[2] - shieldLocations_[1])
                    + (shieldLocations_[1] - shieldLocations_[0])) / ((elapsedTimes_[3] - elapsedTimes_[2]) + 
                    (elapsedTimes_[2] - elapsedTimes_[1]) + (elapsedTimes_[1] - elapsedTimes_[0])) * xVelMod;
        }
        velIndex_++;
        if (velIndex_ > 3) velIndex_ = 0;
    }
}

- (void)update:(ccTime)delta {
    
    // If invisible then act dead.
    if (!self.visible) {
        magnetronTime_ = 0.0;
        magnetronPower_ = 0;
        magnetronPowered_ = FALSE;
        if (state_ == kShieldStateMagnetronOn) {
            state_ = kShieldStateUngrabbed;
            newReaction_ = TRUE;
        }
        return;
    }
    
    if (state_ == kShieldStateMagnetronOn && holdingBall_) {
        magnetronTime_ = magnetronTime_ + delta;
    } else {
        magnetronTime_ = magnetronTime_ - delta / 10.0; // Power regenerates at 1/10 rate it's lost
        if (magnetronTime_ < 0.0) {
            magnetronTime_ = 0.0;
        }
    }
    
    float powerAdjustment = magnetronTime_ * 25.0;  // For every second on lower power by 25.
    magnetronPower_ = 100 - powerAdjustment;
    
    if ((magnetronPower_ >= 50 && !aiPlayer_) || (magnetronPower_ >= 50 && aiPlayer_ && AIMagnetronOn_)) {
        magnetronPowered_ = TRUE;
        [self setColor:ccc3(0, 255, 200)];
        if (state_ == kShieldStateGrabbed) {
            state_ = kShieldStateMagnetronOn;
        }
    } else if (magnetronPower_ < 50) { // Can't catch ball with less than 50% power
        [self setColor:ccc3(255, 0, 0)];
        magnetronPowered_ = FALSE;
    }
    
    if (magnetronPower_ == 0) { // Can't hold ball with 0% power
        magnetronPowered_ = FALSE;
        if (state_ == kShieldStateMagnetronOn) {
            state_ = kShieldStateGrabbed;
            newReaction_ = TRUE;
        }
    }
}

- (void)setAIMagnetronOnTrue:(CCNode *)node {
    AIMagnetronOn_ = TRUE;
}



- (void)wasPaused {
    touchObject1_ = NULL;
    state_ = kShieldStateUngrabbed;
}


- (void)onEnter
{
    [super onEnter];
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
}

- (void)onExit
{
	[[CCTouchDispatcher sharedDispatcher] removeDelegate:self];
	[super onExit];
}	

- (BOOL)containsTouchLocation:(UITouch *)touch magnetronBounds:(CGRect)r 
{
    CGPoint p = [[CCDirector sharedDirector] convertToGL:[touch locationInView:[touch view]]];
    
    //NSLog(@"Touch %@\n Point: %@\n  Bounds %@\n", touch, NSStringFromCGPoint(p), NSStringFromCGRect(r));
	return CGRectContainsPoint(r, p);
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
    if (aiPlayer_) return NO;
    
	//if ( ![self containsTouchLocation:touch] ) return NO;
	CGPoint touchPointRaw = [touch locationInView:[touch view]];
	touchPointRaw = [[CCDirector sharedDirector] convertToGL:touchPointRaw];
    
    //Ignore first 10 pixels at bottom on player side to make both sides touch equivalent 
	if ((((touchPointRaw.y > shieldDefaultYPosition_) || (touchPointRaw.y < 16.0)) && !inverted_) || 
        ((touchPointRaw.y < shieldDefaultYPosition_) && inverted_)) {
        return NO;
    }
    
    if (touchObject1_ != NULL) {
        return NO;
    }
    if (magnetronPowered_) {
        state_ = kShieldStateMagnetronOn;
    } else {
        state_ = kShieldStateGrabbed;
    }
    
    touchOffset1_ = ccpSub(self.position, touchPointRaw);
    touchPoint1_ = self.position; //ccpAdd(touchPointRaw, touchOffset1_);
    touchObject1_ = touch;
    return YES;

}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
	// If it weren't for the TouchDispatcher, you would need to keep a reference
	// to the touch from touchBegan and check that the current touch is the same
	// as that one.
	// Actually, it would be even more complicated since in the Cocos dispatcher
	// you get NSSets instead of 1 UITouch, so you'd need to loop through the set
	// in each touchXXX method.
		
	NSAssert(state_ != kShieldStateUngrabbed, @"Shield - Unexpected state!");	
	if (touch == touchObject1_) {
        CGPoint touchPointRaw = [touch locationInView:[touch view]];
        touchPointRaw = [[CCDirector sharedDirector] convertToGL:touchPointRaw];
        CGPoint touchPointDiff = ccpSub(touchPoint1_, touchPointRaw);
        
        // Make any movement increase in magnitude linearly over time. 
        if (IS_IPAD) {
            if (touchPointDiff.x < touchOffset1_.x) {
                touchOffset1_.x = touchOffset1_.x + 1.065;
            } else {
                touchOffset1_.x = touchOffset1_.x - 1.065;
            }
        } else {
            if (touchPointDiff.x < touchOffset1_.x) {
                touchOffset1_.x = touchOffset1_.x + 0.5;
            } else {
                touchOffset1_.x = touchOffset1_.x - 0.5;
            }
        }
 
        touchPoint1_ = ccpAdd(touchPointRaw, touchOffset1_);
    }
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
	NSAssert(state_ != kShieldStateUngrabbed, @"Shield - Unexpected state!");	
    if (touch == touchObject1_) {
        touchObject1_ = NULL;
        state_ = kShieldStateUngrabbed;
        return;
    }
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
	[self ccTouchEnded:touch withEvent:event];
}

- (void) dealloc
{
    [wait_ dealloc];
    [AIMagnetronOnTrue_ dealloc];
    [magOnSeq_ dealloc];
    [super dealloc];
}

@end
