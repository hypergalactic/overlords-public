//
//  ParticleSystemPoint.h
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//


#import <Availability.h>
#import "ParticleSystem.h"

#define CC_MAX_PARTICLE_SIZE 64

#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED

/** CCParticleSystemPoint is a subclass of CCParticleSystem
 Attributes of a Particle System:
 * All the attributes of Particle System

 Features:
  * consumes small memory: uses 1 vertex (x,y) per particle, no need to assign tex coordinates
  * size can't be bigger than 64
  * the system can't be scaled since the particles are rendered using GL_POINT_SPRITE
 
 Limitations:
  * On 3rd gen iPhone devices and iPads, this node performs MUCH slower than CCParticleSystemQuad.
 */
@interface ParticleSystemPoint : ParticleSystem
{	
	// Array of (x,y,size) 
	ccPointSprite *vertices;
	// vertices buffer id
#if CC_USES_VBO
	GLuint	verticesID;
#endif
}
@end

#elif __MAC_OS_X_VERSION_MAX_ALLOWED

#import "ParticleSystemQuad.h"

@interface ParticleSystemPoint : ParticleSystemQuad
@end

#endif
