//
//  Path.m
//  Overlords
//
//  Created by Robert Wasmann on 11-05-09.
//  Copyright 2011 HyperGalactic Games Corporation. All rights reserved.
//

#import "Path.h"

@implementation Path
+ (NSString *)graphicsPath:(NSString*)filePath
{
	NSString *finalPath = @"GameResources/";
	
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
		finalPath = [finalPath stringByAppendingString:@"ipad/"];
	}
	
	finalPath = [finalPath stringByAppendingString:@"graphics/"];
	return [finalPath stringByAppendingString:filePath];
}

+ (NSString *)resourcePath:(NSString*)filePath
{
	NSString *finalPath = @"GameResources/";
	return [finalPath stringByAppendingString:filePath];
}
@end