//
//  ParticleSystemQuad.h
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//


#import "ParticleSystem.h"
#import "ccConfig.h"

@class CCSpriteFrame;

/** CCParticleSystemQuad is a subclass of CCParticleSystem

 It includes all the features of ParticleSystem.
 
 Special features and Limitations:	
  - Particle size can be any float number.
  - The system can be scaled
  - The particles can be rotated
  - On 1st and 2nd gen iPhones: It is only a bit slower that CCParticleSystemPoint
  - On 3rd gen iPhone and iPads: It is MUCH faster than CCParticleSystemPoint
  - It consumes more RAM and more GPU memory than CCParticleSystemPoint
  - It supports subrects
 @since v0.8
 */
@interface ParticleSystemQuad : ParticleSystem
{
	ccV2F_C4B_T2F_Quad	*quads_;		// quads to be rendered
	GLushort			*indices_;		// indices
#if CC_USES_VBO
	GLuint				quadsID_;		// VBO id
#endif
}

/** initialices the indices for the vertices */
-(void) initIndices;

/** initilizes the texture with a rectangle measured Points */
-(void) initTexCoordsWithRect:(CGRect)rect;

/** Sets a new CCSpriteFrame as particle.
 WARNING: this method is experimental. Use setTexture:withRect instead.
 @since v0.99.4
 */
-(void)setDisplayFrame:(CCSpriteFrame*)spriteFrame;

/** Sets a new texture with a rect. The rect is in Points.
 @since v0.99.4
 */
-(void) setTexture:(CCTexture2D *)texture withRect:(CGRect)rect;

@end

