//
//  GameState.h
//  Overlords
//
//  Created by Robert Wasmann on 12-08-24.
//
//

#import <Foundation/Foundation.h>

@interface GameState : NSObject <NSCoding> {
    BOOL easy;
    BOOL completedLevel1;
    BOOL completedLevel5;
    BOOL completedLevel10;
    BOOL completedLevel25;
    BOOL completedLevel50;
    BOOL completedLevel100;
    BOOL completedLevel1000;

    unsigned int highScoreEasy;
    unsigned int highScoreHard;
            

}

+ (GameState *) sharedInstance;
- (void)save;

@property (assign) BOOL easy;
@property (assign) BOOL completedLevel1;
@property (assign) BOOL completedLevel5;
@property (assign) BOOL completedLevel10;
@property (assign) BOOL completedLevel25;
@property (assign) BOOL completedLevel50;
@property (assign) BOOL completedLevel100;
@property (assign) BOOL completedLevel1000;

@property (assign) unsigned int highScoreEasy;
@property (assign) unsigned int highScoreHard;


@end