//
//  PauseLayer.h
//  Overlords
//
//  Created by Robert Wasmann on 12-06-18.
//  Copyright (c) 2012 HyperGalactic Games Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "OverlordsScene.h"

//@interface PauseLayerProtocol: CCNode

//-(void)pauseLayerDidPause;
//-(void)pauseLayerDidUnpause;
//@end

@interface PauseLayer : CCLayerColor {
    
	OverlordsLayer *delegate_;
    CCMenu *menu_;
}

@property (nonatomic,assign)OverlordsLayer *delegate;

+ (id) layerWithColor:(ccColor4B)color delegate:(OverlordsLayer *)_delegate;
- (id) initWithColor:(ccColor4B)c delegate:(OverlordsLayer *)_delegate;
-(void)pauseDelegate;

@end

