//
//  GCDatabase.h
//  Overlords
//
//  Created by Robert Wasmann on 12-08-24.
//
//

#import <Foundation/Foundation.h>

id loadData(NSString * filename);
void saveData(id theData, NSString *filename);
