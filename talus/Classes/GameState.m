//
//  GameState.m
//  Overlords
//
//  Created by Robert Wasmann on 12-08-24.
//
//

#import "GameState.h"
#import "GCDatabase.h"

@implementation GameState

@synthesize easy;
@synthesize completedLevel1;
@synthesize completedLevel5;
@synthesize completedLevel10;
@synthesize completedLevel25;
@synthesize completedLevel50;
@synthesize completedLevel100;
@synthesize completedLevel1000;

@synthesize highScoreEasy;
@synthesize highScoreHard;


static GameState *sharedInstance = nil;

+(GameState*)sharedInstance {
    @synchronized([GameState class])
    {
        if(!sharedInstance) {
            sharedInstance = [loadData(@"GameState") retain];
            if (!sharedInstance) {
                [[self alloc] init];
            }
        }
        return sharedInstance;
    }
    return nil;
}

+(id)alloc {
    @synchronized ([GameState class])
    {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a \
                 second instance of the GameState singleton");
        sharedInstance = [super alloc];
        return sharedInstance;
    }
    return nil;
}

- (void)save {
    saveData(self, @"GameState");
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeBool:easy forKey:@"easy"];
    [encoder encodeBool:completedLevel1 forKey:@"CompletedLevel1"];
    [encoder encodeBool:completedLevel5 forKey:@"CompletedLevel5"];
    [encoder encodeBool:completedLevel10 forKey:@"CompletedLevel10"];
    [encoder encodeBool:completedLevel25 forKey:@"CompletedLevel25"];
    [encoder encodeBool:completedLevel50 forKey:@"CompletedLevel50"];
    [encoder encodeBool:completedLevel100 forKey:@"CompletedLevel100"];
    [encoder encodeBool:completedLevel1000 forKey:@"CompletedLevel1000"];

    [encoder encodeInt:highScoreEasy forKey:@"highScoreEasy"];
    [encoder encodeInt:highScoreHard forKey:@"highScoreHard"];


}

- (id)initWithCoder:(NSCoder *)decoder {
    if ((self = [super init])) {
        
        easy = [decoder
                           decodeBoolForKey:@"easy"];
        completedLevel1 = [decoder
                           decodeBoolForKey:@"CompletedLevel1"];
        completedLevel5 = [decoder
                           decodeBoolForKey:@"CompletedLevel5"];
        completedLevel10 = [decoder
                           decodeBoolForKey:@"CompletedLevel10"];
        completedLevel25 = [decoder
                           decodeBoolForKey:@"CompletedLevel25"];
        completedLevel50 = [decoder
                           decodeBoolForKey:@"CompletedLevel50"];
        completedLevel100 = [decoder
                           decodeBoolForKey:@"CompletedLevel100"];
        completedLevel1000 = [decoder
                           decodeBoolForKey:@"CompletedLevel1000"];
        
        highScoreEasy = [decoder decodeIntForKey:@"highScoreEasy"];
        highScoreHard = [decoder decodeIntForKey:@"highScoreHard"];

    }
    return self;
}

@end
