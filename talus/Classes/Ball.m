//
//  Ball.m
//  Overlords
//
//  Created by Robert Wasmann on 11-08-07.
//  Copyright HyperGalactic Games Corporation 2011. All rights reserved.
//

#import "Ball.h"
#import "Shield.h"

#define controlBoxRatio 0.04166f // Height ratio of the controller box
#define startVelocity 400.0f

@implementation Ball

@synthesize active = active_;
@synthesize ball1 = ball1_;
@synthesize velocity = velocity_;
@synthesize scalarVelocity = scalarVelocity_;
@synthesize ticksSinceLastCollision = ticksSinceLastCollision_;
@synthesize brickImpactsTillDeath = brickImpactsTillDeath_;
@synthesize brickImpacts = brickImpacts_;
@synthesize heldByEnemyShield = heldByEnemyShield_;
@synthesize justHeldByEnemyShield = justHeldByEnemyShield_;
@synthesize heldByPlayerShield = heldByPlayerShield_;
@synthesize justHeldByPlayerShield = justHeldByPlayerShield_;
@synthesize shieldHolding = shieldHolding_;
@synthesize myPlasmaBall = myPlasmaBall_;

-(float)radius
{
	return self.texture.contentSize.width / 2.0;
}

+ (id)ballWithTexture:(CCTexture2D *)aTexture
{
	return [[[self alloc] initWithTexture:aTexture] autorelease];
}

-(float)randomStart
{
	CGSize winSize = [[CCDirector sharedDirector] winSize];
	float velocityAngle;
	scalarVelocity_ = startVelocity * winSize.height / 480.0; //keep speed constant relative to iPhone
	int updown = arc4random() % 100;
	
	if (updown > 49){   // initially ball goes up
		int randRad = (arc4random() % 245) + 35;
		velocityAngle = randRad / 100.00;	
	} else {            // initially ball goes down
		int randRad = (arc4random() % 245) + 349;
		velocityAngle = randRad / 100.00;
	}
	//velocityAngle = 0.0;
	velocity_ = ccpMult(ccpForAngle(velocityAngle), scalarVelocity_);
    
    return velocityAngle;
}

- (void)reset {
    visible_ = FALSE;
    active_ = FALSE;
    ticksSinceLastCollision_ = 100;
    heldByEnemyShield_ = FALSE;
    justHeldByEnemyShield_ = FALSE;
    heldByPlayerShield_ = FALSE;
    justHeldByPlayerShield_ = FALSE;
    shieldHolding_ = nil;
    brickImpactsTillDeath_ = brickImpacts_;
}

- (void)move:(ccTime)delta
{
	CGSize winSize = [[CCDirector sharedDirector] winSize];
	// calculate the scaled height for the control area
	float controlHeight = controlBoxRatio * winSize.height; 

    
    // Prevent ball from moving too far when framerate drops to super low levels.
    float maxDelta = 1.0 / 20.0;
    
    if (delta > maxDelta) {
        self.position = ccpAdd(self.position, ccpMult(velocity_, maxDelta));
    } else {
        self.position = ccpAdd(self.position, ccpMult(velocity_, delta));
    }
    
    if (heldByPlayerShield_) {
        justHeldByPlayerShield_ = TRUE;
    } else if (heldByEnemyShield_) {
        justHeldByEnemyShield_ = TRUE;
    }
	
	//right boundary
	if (self.position.x > winSize.width/2.0 + winSize.height/3.0 - self.radius) {
		[self setPosition: ccp(winSize.width/2.0 + winSize.height/3.0 - self.radius, self.position.y)];
        [self randomness];
		velocity_.x *= -1.0;
        
	//left boundary
	} else if (self.position.x < self.radius + winSize.width/2.0 - winSize.height/3.0) {
		[self setPosition: ccp(self.radius + winSize.width/2.0 - winSize.height/3.0, self.position.y)];
        [self randomness];
		velocity_.x *= -1.0;

	//top boundary
	} else if (self.position.y > winSize.height - controlHeight - self.radius) {
		[self setPosition: ccp(self.position.x, winSize.height - controlHeight - self.radius)];
        [self randomness];
        justHeldByPlayerShield_ = FALSE;
        justHeldByEnemyShield_ = FALSE;
		velocity_.y *= -1.0;

	//bottom boundary
	} else if (self.position.y < self.radius + controlHeight) {
		[self setPosition: ccp(self.position.x, self.radius + controlHeight)];
        [self randomness];
        justHeldByPlayerShield_ = FALSE;
        justHeldByEnemyShield_ = FALSE;
		velocity_.y *= -1.0;

	}
}

- (void)randomness 
{
    float velocityAngle = ccpToAngle(velocity_);
    scalarVelocity_ = ccpLength(velocity_); // Magnitude of velocity vector
    float randRad;
    
    randRad = (arc4random() % 10);
    randRad = (randRad - 5 ) / 100;
    velocityAngle  += randRad;
    
    // disallow small angle wall bounces
    if ((velocityAngle > 2.942 && velocityAngle < -2.942) || (velocityAngle > -0.2 && velocityAngle < 0.2))
    {
        //NSLog(@"velocityAngleOrg: %f velocityAngle: %f", velocityAngleOrg, velocityAngle);

        if (velocityAngle > 0 || velocityAngle < -2.942)
        {
            velocityAngle += 0.1;
            //NSLog(@"Small angle increased:, %f", velocityAngle);

        } else
        {
            velocityAngle -= 0.1;
            //NSLog(@"Small angle decreased:, %f", velocityAngle);

        }
    } 
    velocity_ = ccpMult(ccpForAngle(velocityAngle), scalarVelocity_);
}

- (void)setVelocityAngle:(float)velocityAngle
{
    if (scalarVelocity_ == 0) {
        scalarVelocity_ = startVelocity;
    }
    velocity_ = ccpMult(ccpForAngle(velocityAngle), scalarVelocity_);
}

- (void)addXVelocity:(float)xVelocity {
    
    velocity_ = ccpAdd(velocity_, ccp(xVelocity, 0.0));
    scalarVelocity_ = ccpLength(velocity_);
    
}

- (void)setScalarVelocity:(float)scalarVelocity {
    
    scalarVelocity_ = scalarVelocity;
    float velocityAngle = ccpToAngle(velocity_);
    velocity_ = ccpMult(ccpForAngle(velocityAngle), scalarVelocity_);
    
}

- (BOOL)collideWithShield:(Shield *)Shield
{

    BOOL contact = FALSE;
    
    if (!Shield.visible) {
        return contact;
    }
	if ((velocity_.y < 0 && !Shield.inverted) || (velocity_.y > 0 && Shield.inverted)){
		if (ccpLengthSQ(ccpSub(self.position, Shield.position)) < 
            ((self.radius + Shield.initTextureRadius)*(self.radius + Shield.initTextureRadius))){
            
            [self randomness];
			velocity_.y *= -1.0;
            contact = TRUE;
            
            if (Shield.inverted) {
                justHeldByEnemyShield_ = TRUE;
                justHeldByPlayerShield_ = FALSE;
            } else {
                justHeldByPlayerShield_ = TRUE;
                justHeldByEnemyShield_ = FALSE;
            }
		}
	}
    return contact;
}

@end
